﻿using System;
using System.Globalization;
using Orpyx.Si.Common.Dates;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Date in format MM/dd/yyyy
        /// </summary>
        public static DateTime ParseAmericanDateTime(this string str)
        {
            var dateTime = DateTime.ParseExact(str, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            return dateTime;
        }

        public static DateTimeOffset ParseAmericanDateTime(this string str, TimeSpan timeZoneOffset)
        {
            return str.ParseAmericanDateTime().WithTimeZone(timeZoneOffset);
        }
    }
}