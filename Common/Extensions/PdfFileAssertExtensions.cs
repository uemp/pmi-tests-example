﻿using System;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore.Internal;
using NUnit.Framework;
using Orpyx.Si.Common.HttpClient;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class PdfFileAssertExtensions
    {
        private static string PDF_START_SIGNATURE { get; } = "%PDF-";
        private static string PDF_END_SIGNATURE { get; } = $"%%EOF{Environment.NewLine}";

        private static string[] PDF_META_TAGS { get; } = new[]
        {
            "obj", "endobj",
            "<<", ">>",
            "/ViewerPreferences",
            "/Type",
            "/Pages",
            "/ProcSet",
            "/Length ",
            "/Encoding"
        };

        public static void AssertPDFFile(this HttpClientFileResponse response, string errorMessage)
        {
            response.Content.AssertIsNotNullAndNotEmpty($"{errorMessage}, failed read pdf, content is empty");

            var pdfStringContent = response.GetStringContent();
            Assert.IsTrue(pdfStringContent.StartsWith(PDF_START_SIGNATURE), $"{errorMessage}, Invalid PDF File begin signature");
            Assert.IsTrue(pdfStringContent.EndsWith(PDF_END_SIGNATURE), $"{errorMessage}, Invalid PDF File end signature");

            foreach (var tag in PDF_META_TAGS)
            {
                Assert.IsTrue(pdfStringContent.Contains(tag), $"{errorMessage}, Invalid PDF File, tag {tag} not found");
            }
        }
    }
}