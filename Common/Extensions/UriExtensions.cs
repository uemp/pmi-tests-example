﻿using System;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class UriExtensions
    {
        public static string EscapeDataString(this string str)
        {
            var escapedStr = Uri.EscapeDataString(str);
            return escapedStr;
        }
    }
}