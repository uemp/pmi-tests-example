﻿using NUnit.Framework;
using Orpyx.Si.Domain.ValueObjects;

namespace Orpyx.Si.Tests.Common.Extensions.Asserts
{
    public static class ValueObjectsAsserts
    {
        public static void AssertModel(this Email email, string errorMessage)
        {
            Assert.IsNotNull(email, errorMessage);

            Assert.IsNotNull(email.Value, $"{errorMessage} | Invalid {nameof(Email)}");
            Assert.IsNotEmpty(email.Value, $"{errorMessage} | Invalid {nameof(Email)}");

            Assert.IsTrue(email.Value.Contains("@"), $"{errorMessage} | Invalid {nameof(Email)}");
            Assert.IsTrue(email.Value.Contains("."), $"{errorMessage} | Invalid {nameof(Email)}");
        }

        public static void AssertModel(this Address address, string errorMessage)
        {
            Assert.IsNotNull(address, errorMessage);

            Assert.IsNotNull(address.City, $"{errorMessage} | Invalid {nameof(Address.City)}");
            Assert.IsNotEmpty(address.City, $"{errorMessage} | Invalid {nameof(Address.City)}");

            Assert.IsNotNull(address.State, $"{errorMessage} | Invalid {nameof(Address.State)}");
            Assert.IsNotEmpty(address.State, $"{errorMessage} | Invalid {nameof(Address.State)}");

            Assert.IsNotNull(address.Street, $"{errorMessage} | Invalid {nameof(Address.Street)}");
            Assert.IsNotEmpty(address.Street, $"{errorMessage} | Invalid {nameof(Address.Street)}");

            Assert.IsNotNull(address.ZipCode, $"{errorMessage} | Invalid {nameof(Address.ZipCode)}");
            Assert.IsNotEmpty(address.ZipCode, $"{errorMessage} | Invalid {nameof(Address.ZipCode)}");
        }
    }
}