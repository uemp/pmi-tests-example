﻿using NUnit.Framework;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Common.Operations.Codes;

namespace Orpyx.Si.Tests.Common.Extensions.Asserts
{
    public static class OperationResultAsserts
    {
        public static void AssertIsSucceeded(this OperationResult result, string errorMessage = default)
        {
            Assert.IsTrue(result.IsSucceeded, $"{errorMessage} | IsFailed | error: {result.Error}");
            Assert.IsNull(result.Error, $"{errorMessage} | Has exist error: {result.Error}");
        }      
        
        public static void AssertIsSucceededOrSkipped(this OperationResult result, string errorMessage = default)
        {
            Assert.IsTrue(result.IsSucceededOrSkipped, $"{errorMessage} | IsFailed | error: {result.Error}");
        }

        public static void AssertIsSucceeded<T>(this OperationResult<T> result, string errorMessage = default)
            where T : class
        {
            Assert.IsTrue(result.IsSucceeded, $"{errorMessage} | IsFailed | error: {result.Error}");
            Assert.IsNotNull(result.Result, $"{errorMessage} | error: {result.Error}");

            Assert.IsNull(result.Error, $"{errorMessage} | Has exist error: {result.Error}");
        }

        public static void AssertIsFailed(this OperationResult result, string errorMessage = default)
        {
            Assert.IsTrue(result.IsFailed, $"{errorMessage} | IsSucceeded");
            Assert.IsNotNull(result.Error, $"{errorMessage}");
        }

        public static void AssertIsFailed(this OperationResult result, OperationResultCode code, string errorMessage = default)
        {
            result.AssertIsFailed(errorMessage);

            Assert.AreEqual(result.Error.Code, code, $"{errorMessage} | Invalid code");
        }
    }
}