﻿using System;
using System.IO;
using System.IO.Compression;
using NUnit.Framework;
using Orpyx.Si.Common.HttpClient;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class FileAssertExtensions
    {
        public static void AssertZipFile(this HttpClientFileResponse response, string errorMessage)
        {
            response.Content.AssertIsNotNullAndNotEmpty($"{errorMessage}, failed read zipFile, content is empty");

            try
            {
                using var stream = response.GetMemoryStreamContent();
                using var zipFile = new ZipArchive(stream);

                Assert.IsNotNull(zipFile.Entries, $"{errorMessage}, failed read zipFile.Entries");
                Assert.IsNotEmpty(zipFile.Entries, $"{errorMessage}, failed read zipFile.Entries");
            }
            catch (Exception e)
            {
                Assert.Fail($"{errorMessage}, failed read zipFile | Exception: {e}");
            }
        }
    }
}