﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using Orpyx.AspNet.Common.Serialization.Json;
using Orpyx.Si.Common.HttpClient;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Common.Operations.Codes;
using Orpyx.Si.Common.Pagination;
using Orpyx.Si.Common.Serialization.Json;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class AssertExtensions
    {
        private static ICommonJsonSerializer JsonSerializer { get; } = new CommonJsonSerializer();

        public static void AssertIsNotNullAndNotEmpty<T>(this IEnumerable<T> collection, string errorMessage)
        {
            Assert.IsNotNull(collection, $"{errorMessage}, collection is null");
            Assert.IsNotEmpty(collection, $"{errorMessage}, collection is empty");
        }

        public static void AssertIsNotNullAndNotEmpty<T>(this string str, string errorMessage)
        {
            Assert.IsNotNull(str, $"{errorMessage}, collection is null");
            Assert.IsNotEmpty(str, $"{errorMessage}, collection is empty");
        }

        public static void AssertIsCorrect(this HttpClientResponse response, string errorMessage)
        {
            Assert.True(response.IsSuccessful, $"{errorMessage} | StatusCode: {response.StatusCode} | Content: {response.Content}");
        }     
        
        public static void AssertModel<T>(this PagingResult<T> result, string errorMessage)
        {
            Assert.IsNotNull(result, errorMessage);
            
            Assert.GreaterOrEqual(result.Total, 0, $"{errorMessage} | Invalid {nameof(PagingResult<T>.Total)}");
            Assert.IsNotNull(result.Items, $"{errorMessage} | Invalid {nameof(PagingResult<T>.Items)}");
        }

        public static void AssertIsCorrect(this HttpClientFileResponse response, string errorMessage)
        {
            Assert.True(response.IsSuccessful, $"{errorMessage} | StatusCode: {response.StatusCode} | HasContent: {response.Content != null && response.Content.Any()}");
        }

        public static void AssertIsNotNull<T>(this HttpClientResponse<T> response, string errorMessage) where T : class
        {
            response.AssertIsCorrect(errorMessage);
            Assert.NotNull(response.Response, $"Failed deserialize response, content: {response.Content}");
        }


        public static void AssertIsNotCorrect(this HttpClientResponse response, HttpStatusCode httpStatusCode, string errorMessage)
        {
            Assert.AreEqual(httpStatusCode, response.StatusCode, $"{errorMessage}, content: {response.Content}");
        }

        public static void AssertIsNotCorrect(this HttpClientResponse response, HttpStatusCode httpStatusCode, OperationResultCode operationResultCode, string errorMessage)
        {
            response.AssertIsNotCorrect(httpStatusCode, errorMessage);

            var operationError = JsonSerializer.DeserializeObject<OperationError>(response.Content);
            Assert.NotNull(operationError, $"Failed deserialize {httpStatusCode}, content: {response.Content}");
            Assert.AreEqual(operationResultCode, operationError.Code, $"{errorMessage}, content: {response.Content}");
        }

        public static void AssertIsNotCorrect(this HttpClientResponse response, OperationResultCode operationResultCode, string errorMessage)
        {
            var operationError = JsonSerializer.DeserializeObject<OperationError>(response.Content);
            Assert.NotNull(operationError, $"Failed deserialize {response.StatusCode}, content: {response.Content}");
            Assert.AreEqual(operationError.Code, operationResultCode, errorMessage);
        }
    }
}