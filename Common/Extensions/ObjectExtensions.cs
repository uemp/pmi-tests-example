﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orpyx.Si.Tests.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static Dictionary<string, string> GetProperties(this object obj)
        {
            if (obj == null)
                return new Dictionary<string, string>();

            return obj
                   .GetType()
                   .GetProperties()
                   .Where(property => property.CanRead && property.GetValue(obj) != null)
                   .ToDictionary(property => property.Name, property => property.GetValue(obj)?.ToString() ?? string.Empty);
        }
    }
}
