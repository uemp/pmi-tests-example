﻿using System;
using System.Security.Cryptography;
using System.Threading;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    internal static class RandomProvider
    {
        private static RNGCryptoServiceProvider CryptoServiceProvider { get; } = new RNGCryptoServiceProvider();
        private static int seed = Environment.TickCount;
     
        private static readonly ThreadLocal<Random> RandomWrapper = new ThreadLocal<Random>(() =>
        {
            byte[] seedBytes = new byte[4];
            CryptoServiceProvider.GetNonZeroBytes(seedBytes);

            var randomSeed = BitConverter.ToInt32(seedBytes);
            return new Random(randomSeed + Interlocked.Increment(ref seed));
        });

        public static Random GetThreadRandom()
        {
            return RandomWrapper.Value;
        }
    }
}