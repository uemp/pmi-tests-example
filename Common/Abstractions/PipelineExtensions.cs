﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Common.Reflection;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    public static class PipelineExtensions
    {
        private static Type[] PipelineTypes { get; } = new[]
        {
            typeof(IPipelineClient),
            typeof(IPipelineGenerator),
            typeof(IPipelineService),
            typeof(IPipelineServiceContainer),
            typeof(IPipelineStorage),
        };

        public static IServiceCollection AddPipelineServices(this IServiceCollection services, Assembly assembly)
        {
            var pipelineServices = assembly.GetTypesImplementedInterfaces(PipelineTypes).Where(q => !q.IsAbstract).ToArray();

            foreach (var service in pipelineServices)
            {
                services.AddSingleton(service);

                var serviceInterfaces = service.GetInterfaces();
                foreach (var @interface in serviceInterfaces.Where(type => PipelineTypes.Contains(type)))
                {
                    services.AddSingleton(@interface, provider => provider.GetRequiredService(service));
                }
            }

            return services;
        }
    }
}