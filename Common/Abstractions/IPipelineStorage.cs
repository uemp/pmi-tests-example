﻿using System.Threading.Tasks;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    public interface IPipelineStorage
    {
        Task Setup(IBaseActorExecutor executor);
    }
}