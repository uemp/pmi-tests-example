﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    public class EnumArrayEqualityComparer<TEntity> : IEqualityComparer<TEntity[]>
        where TEntity : Enum
    {
        public static EnumArrayEqualityComparer<TEntity> Comparer { get; } = new EnumArrayEqualityComparer<TEntity>();

        public bool Equals(TEntity[] x, TEntity[] y)
        {
            if (x.Length != y.Length)
                return false;

            var xx = x.OrderBy(q => q).ToArray();
            var yy = y.OrderBy(q => q).ToArray();

            for (var i = 0; i < x.Length; i++)
            {
                if (!xx[i].Equals(yy[i]))
                    return false;
            }

            return true;
        }

        public int GetHashCode(TEntity[] obj)
        {
            return obj.Sum(q => Convert.ToInt32((object) q));
        }
    }
}