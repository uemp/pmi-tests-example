﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Tests.Common.Mocks.CurrentDate;

namespace Orpyx.Si.Tests.Common.Abstractions.Tests
{
    public abstract class AbstractTestStartup<TTestStartup> where TTestStartup :AbstractTestStartup<TTestStartup>
    {
        public static IWebHostEnvironment Environment { get; private set; }
        public static IConfiguration Configuration { get; private set; }
        public static IServiceProvider ServiceProvider { get; private set; }

        public AbstractTestStartup(IWebHostEnvironment environment)
        {
            Environment = environment;
            Configuration = ConfigureConfigurationBuilder(new ConfigurationBuilder(), environment).Build();
        }

        private IConfigurationBuilder ConfigureConfigurationBuilder(IConfigurationBuilder builder, IWebHostEnvironment environment)
        {
            var currentPath = Directory.GetCurrentDirectory();

            return builder.SetBasePath(currentPath)
                .AddEnvironmentVariables(environment.EnvironmentName)
                .AddJsonFile("appsettings.json", false)
                .AddJsonFile("appsettings.Development.json", true)
                .AddEnvironmentVariables();
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddMockCurrentDateProvider();
            services.AddHostedService<PipelineStorageInitializationService>();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ServiceProvider = app.ApplicationServices;
        }
    }
}