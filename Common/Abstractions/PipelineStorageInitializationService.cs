﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    internal sealed class PipelineStorageInitializationService : BackgroundService
    {
        private IServiceProvider ServiceProvider { get; }
        private IBaseActorExecutor Executor { get; }

        public PipelineStorageInitializationService(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
            Executor = new DefaultActorExecutor(ServiceProvider);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var storages = ServiceProvider
                .GetServices<IPipelineStorage>()
                .ToArray();

            foreach (var storage in storages)
            {
                await storage.Setup(Executor);
            }
        }
    }
}