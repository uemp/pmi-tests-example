﻿using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Tests.Common.Abstractions
{
    public abstract class PipelineServiceContainer<TClient, TService> : IPipelineServiceContainer
        where TClient : class, IPipelineClient
        where TService : class, IPipelineService
    {
        public TClient Client { get; }
        public TService Service { get; }

        protected PipelineServiceContainer(TClient client, TService service)
        {
            Client = client;
            Service = service;
        }
    }

    public abstract class PipelineServiceContainer<TClient, TService, TGenerators> : PipelineServiceContainer<TClient, TService>
        where TService : class, IPipelineService
        where TClient : class, IPipelineClient
        where TGenerators : class, IPipelineGenerator
    {
        public TGenerators Generators { get; }

        protected PipelineServiceContainer(TClient client, TService service, TGenerators generators) : base(client, service)
        {
            Generators = generators;
        }
    }
}