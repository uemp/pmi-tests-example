﻿using System;
using System.Threading.Tasks;
using Orpyx.Si.Common.MediatR;
using Orpyx.Si.Tests.Common.Actors.Actions;

namespace Orpyx.Si.Tests.Common.Actors
{
    public interface IBaseActorExecutor
    {
        IOperationExecutor OperationExecutor { get; }
        
        IActorExecutorActions ActionBeforeExecute { get; }
        IActorExecutorActions ActionAfterExecute { get; }
        
        void ExecuteInScope(Action<IServiceProvider> action);
        T ExecuteInScope<T>(Func<IServiceProvider, T> action);

        Task ExecuteInScope(Func<IServiceProvider, Task> action);
        Task<T> ExecuteInScope<T>(Func<IServiceProvider, Task<T>> action);
    }
    
    public interface IBaseActorExecutor<out TActor> : IOperationExecutor, IBaseActorExecutor
        where TActor : class
    {
        TActor Actor { get; }
    }
}