﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Common.MediatR;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Tests.Common.Actors.Actions;

namespace Orpyx.Si.Tests.Common.Actors
{
    public abstract class BaseActorExecutor : IBaseActorExecutor
    {
        private IServiceProvider ServiceProvider { get; }
        public IOperationExecutor OperationExecutor { get; }
        public IActorExecutorActions ActionBeforeExecute { get; }
        public IActorExecutorActions ActionAfterExecute { get; }
        
        protected BaseActorExecutor(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
            OperationExecutor = new ActorOperationExecutor(this);
            ActionAfterExecute = new ActorExecutorActions(this);
            ActionBeforeExecute = new ActorExecutorActions(this);
        }

        protected abstract void ConfigureServiceProvider(IServiceProvider serviceProvider);

        public void ExecuteInScope(Action<IServiceProvider> action)
        {
            using var scope = ServiceProvider.CreateScope();
            ConfigureServiceProvider(scope.ServiceProvider);

            ActionBeforeExecute.ExecuteActions(scope.ServiceProvider);
            action.Invoke(scope.ServiceProvider);
            ActionAfterExecute.ExecuteActions(scope.ServiceProvider);
        }

        public T ExecuteInScope<T>(Func<IServiceProvider, T> action)
        {
            using var scope = ServiceProvider.CreateScope();
            ConfigureServiceProvider(scope.ServiceProvider);

            ActionBeforeExecute.ExecuteActions(scope.ServiceProvider);
            var result = action.Invoke(scope.ServiceProvider);
            ActionAfterExecute.ExecuteActions(scope.ServiceProvider);

            return result;
        }

        public async Task ExecuteInScope(Func<IServiceProvider, Task> action)
        {
            using var scope = ServiceProvider.CreateScope();
            ConfigureServiceProvider(scope.ServiceProvider);

            ActionBeforeExecute.ExecuteActions(scope.ServiceProvider);
            await action.Invoke(scope.ServiceProvider);
            ActionAfterExecute.ExecuteActions(scope.ServiceProvider);
        }

        public async Task<T> ExecuteInScope<T>(Func<IServiceProvider, Task<T>> action)
        {
            using var scope = ServiceProvider.CreateScope();
            ConfigureServiceProvider(scope.ServiceProvider);

            ActionBeforeExecute.ExecuteActions(scope.ServiceProvider);
            var result = await action.Invoke(scope.ServiceProvider);
            ActionAfterExecute.ExecuteActions(scope.ServiceProvider);
            
            return result;
        }

        public Task<OperationResult> Execute(IOperationRequest request, CancellationToken cancellationToken)
        {
            return OperationExecutor.Execute(request, cancellationToken);
        }

        public Task<OperationResult<TResult>> Execute<TResult>(IOperationRequest<TResult> request, CancellationToken cancellationToken)
        {
            return OperationExecutor.Execute(request, cancellationToken);
        }
    }
    
    public abstract class BaseActorExecutor<TActor> : BaseActorExecutor, IBaseActorExecutor<TActor>
        where TActor : class
    {
        public TActor Actor { get; }

        protected BaseActorExecutor(IServiceProvider serviceProvider, TActor actor) : base(serviceProvider)
        {
            Actor = actor;
        }
    }
}