﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Common.MediatR;
using Orpyx.Si.Common.Operations;

namespace Orpyx.Si.Tests.Common.Actors
{
    internal sealed class ActorOperationExecutor : IOperationExecutor
    {
        private IBaseActorExecutor Root { get; }

        public ActorOperationExecutor(IBaseActorExecutor actorExecutor)
        {
            Root = actorExecutor;
        }

        public Task<OperationResult> Execute(IOperationRequest request, CancellationToken cancellationToken)
        {
            return Root.ExecuteInScope(async serviceProvider =>
            {
                var operationExecutor = serviceProvider.GetRequiredService<IOperationExecutor>();
                
                //    we need to await for the completion of the task in order to free resources after the task
                return await operationExecutor.Execute(request, cancellationToken);
            });
        }

        public Task<OperationResult<TResult>> Execute<TResult>(IOperationRequest<TResult> request, CancellationToken cancellationToken)
        {
            return Root.ExecuteInScope(async serviceProvider =>
            {
                var operationExecutor = serviceProvider.GetRequiredService<IOperationExecutor>();
                //    we need to await for the completion of the task in order to free resources after the task
                return await operationExecutor.Execute(request, cancellationToken);
            });
        }
    }
}