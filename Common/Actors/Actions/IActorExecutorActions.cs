﻿using System;

namespace Orpyx.Si.Tests.Common.Actors.Actions
{
    public interface IActorExecutorActions
    {
        Guid? OverrideCurrentDateActionId { get; }

        Guid OverrideCurrentDateAction(DateTime dateTime);
        Guid OverrideCurrentDateAction(DateTimeOffset dateTime);     

        Guid OverrideCurrentDateAction(Func<DateTimeOffset> dateTimeSelector);

        Guid AddAction(Action<IServiceProvider> action);
        bool RemoveAction(Guid actionId);
        bool ReplaceAction(Guid actionId, Action<IServiceProvider> action);

        void ClearActions();
        void ExecuteActions(IServiceProvider serviceProvider);
    }
}