﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Tests.Common.Mocks.CurrentDate;

namespace Orpyx.Si.Tests.Common.Actors.Actions
{
    internal class ActorExecutorActions : IActorExecutorActions
    {
        public Guid? OverrideCurrentDateActionId { get; set; }

        private IBaseActorExecutor Actor { get; }
        private Dictionary<Guid, Action<IServiceProvider>> Actions { get; }

        public ActorExecutorActions(IBaseActorExecutor actor)
        {
            Actor = actor;
            Actions = new Dictionary<Guid, Action<IServiceProvider>>();
        }

        public Guid OverrideCurrentDateAction(DateTime dateTime)
        {
            return OverrideCurrentDateAction(new DateTimeOffset(dateTime, TimeSpan.Zero));
        }

        public Guid OverrideCurrentDateAction(DateTimeOffset dateTime)
        {
            if (OverrideCurrentDateActionId.HasValue)
            {
                RemoveAction(OverrideCurrentDateActionId.Value);
            }
            
            OverrideCurrentDateActionId = AddAction(provider =>
            {
                var source = provider.GetRequiredService<MockCurrentDateProvider>();
                source.Now = dateTime;
            });

            return OverrideCurrentDateActionId.Value;
        }
        
        public Guid OverrideCurrentDateAction(Func<DateTimeOffset> dateTimeSelector)
        {
            Assert.IsNotNull(dateTimeSelector, $"Invalid {nameof(dateTimeSelector)}");

            if (OverrideCurrentDateActionId.HasValue)
            {
                RemoveAction(OverrideCurrentDateActionId.Value);
            }
            
            OverrideCurrentDateActionId = AddAction(provider =>
            {
                var source = provider.GetRequiredService<MockCurrentDateProvider>();
                source.Now = dateTimeSelector.Invoke();
            });

            return OverrideCurrentDateActionId.Value;
        }

        public Guid AddAction(Action<IServiceProvider> action)
        {
            var id = Guid.NewGuid();
            Actions.Add(id, action);
            return id;
        }

        public bool RemoveAction(Guid actionId)
        {
            if (OverrideCurrentDateActionId == actionId)
            {
                OverrideCurrentDateActionId = null;
            }

            return Actions.Remove(actionId);
        }

        public bool ReplaceAction(Guid actionId, Action<IServiceProvider> action)
        {
            if (Actions.TryGetValue(actionId, out _))
            {
                Actions.Remove(actionId);
                Actions.Add(actionId, action);
                return true;
            }

            return false;
        }

        public void ClearActions()
        {
            OverrideCurrentDateActionId = null;
            Actions.Clear();
        }

        public void ExecuteActions(IServiceProvider serviceProvider)
        {
            foreach (var action in Actions)
            {
                action.Value.Invoke(serviceProvider);
            }
        }
    }
}