﻿using System;

namespace Orpyx.Si.Tests.Common.Actors
{
    public sealed class DefaultActorExecutor : BaseActorExecutor
    {
        public DefaultActorExecutor(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        protected override void ConfigureServiceProvider(IServiceProvider serviceProvider)
        {
            
        }
    }
}