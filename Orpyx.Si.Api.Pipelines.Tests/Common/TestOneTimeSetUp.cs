﻿using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Tests.Common;
using EnvironmentName = Microsoft.Extensions.Hosting.EnvironmentName;

#pragma warning disable CS0618 // Type or member is obsolete
// ReSharper disable once CheckNamespace
// Required class without namepsace to initialize just once
namespace Orpyx.Si.Api.Pipelines.Tests
{
    [SetUpFixture]
    [DebuggerStepThrough]
    [Parallelizable(scope: ParallelScope.All)]
    internal sealed class TestOneTimeSetUp
    {
        private TestServer TestServer { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            TestServer = new TestServer(new WebHostBuilder()
                .UseEnvironment(EnvironmentName.Development)
                .CaptureStartupErrors(true)
                .UseStartup<TestStartup>());
        }
    }
}