﻿using System;
using IngestorProcessor.Common.Tests.Wrappers;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Tests.Wrappers;
using Orpyx.Si.Api.Pipelines.Tests.Wrappers.AzureFunctions;

[assembly:LevelOfParallelism(16)]
namespace Orpyx.Si.Api.Pipelines.Tests.Common
{
    internal abstract class BaseTest
    {
        private IServiceProvider ServiceProvider { get; set; }
        protected AdminServicesContainer Admin { get; private set; }
        protected DoctorsServicesContainer Doctors { get; private set; }
        protected IngestorProcessorServicesContainer IngestorProcessor { get; private set; }
        protected AzureFunctionsServicesContainer AzureFunctions { get; private set; }

        protected T GetRequiredService<T>()
        {
            return ServiceProvider.GetRequiredService<T>();
        }


        [OneTimeSetUp]
        public void Setup()
        {
            Assert.IsNotNull(TestStartup.Configuration, $"{nameof(TestStartup.Configuration)} not initialized in {nameof(TestOneTimeSetUp)}");
            Assert.IsNotNull(TestStartup.ServiceProvider, $"{nameof(TestStartup.ServiceProvider)} not initialized in {nameof(TestOneTimeSetUp)}");

            ServiceProvider = TestStartup.ServiceProvider;

            Admin = ServiceProvider.GetRequiredService<AdminServicesContainer>();
            Doctors = ServiceProvider.GetRequiredService<DoctorsServicesContainer>();
            IngestorProcessor = ServiceProvider.GetRequiredService<IngestorProcessorServicesContainer>();
            AzureFunctions = ServiceProvider.GetRequiredService<AzureFunctionsServicesContainer>();
        }
    }
}