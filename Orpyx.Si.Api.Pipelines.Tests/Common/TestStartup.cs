using ActionRequiredAlerts.Adherence.Extensions;
using ActionRequiredAlerts.Pressure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Admin.Mocks.Users;
using Orpyx.Si.Api.Admin.Extensions;
using Orpyx.Si.Api.Pipelines.Common.Extensions;
using Orpyx.Si.Admin.Pipelines.Common.Extensions;
using Orpyx.Si.Admin.Services;
using Orpyx.Si.Admin.Services.Confirmation;
using Orpyx.Si.Api.Pipelines.Tests.Wrappers;
using Orpyx.Si.Api.Pipelines.Tests.Wrappers.AzureFunctions;
using Orpyx.Si.Api.Server.Extensions;
using Orpyx.Si.Domain.OrpyxUsers;
using Orpyx.Si.Tests.Common.Abstractions.Tests;
using Orpyx.Si.Tests.Common.Mocks.CurrentDate;
using TemperatureDifference.TemperatureCalculator.Extensions;
using IngestorProcessor.Common.Tests.Extensions;
using Orpyx.IngestorProcessor.Extensions;
using Orpyx.Si.Tests.Common.Mocks.PhiLogs;
using PatientStatisticSummaryPusher.Extensions;
using TemperatureDifference.TemperatureCalculator.Triggers;
using TemperatureReviewAlert.Extensions;

namespace Orpyx.Si.Api.Pipelines.Tests.Common
{
    internal sealed class TestStartup : AbstractTestStartup<TestStartup>
    {
        public TestStartup(IWebHostEnvironment environment) : base(environment)
        {
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddIngestorProcessorServices(Configuration);

            ConfigureAdminServices(services);
            ConfigureDoctorsServices(services);
            ConfigureIngestorServices(services);
            ConfigureAzureFunctions(services);
            
            services.AddMockCurrentDateProvider();
            services.AddHttpPhiAccessPointResolverMock();
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);
            app.UseAdminActorExecutorBuilder();
        }

        private void ConfigureDoctorsServices(IServiceCollection services)
        {
            services.AddDoctorsPipelinesServices();
            services.AddDoctorsDashboardServices(Configuration);
            services.AddSingleton<DoctorsServicesContainer>();

            services.AddCarePractitionerIdentifierResolver();
        }

        private void ConfigureIngestorServices(IServiceCollection services)
        {
            services.AddIngestorProcessorPipelineServices();
        }

        private void ConfigureAzureFunctions(IServiceCollection services)
        {
            services.AddActionRequiredAlertsPressureServices(Configuration);
            services.AddTemperatureDifferenceTemperatureCalculatorServices(Configuration);

            services.AddPatientStatisticSummaryPusherServices(Configuration);
            services.AddPatientAdherenceProcessServices(Configuration);

            services.AddPatientTemperatureReviewAlertServices(Configuration);

            services.AddSingleton<AzureFunctionsServicesContainer>();
        }

        private void ConfigureAdminServices(IServiceCollection services)
        {
            services.AddAdminServices(Configuration, Environment);
            services.AddAdminPipelinesServices();

            services.AddSingleton<AdminServicesContainer>();
            services.AddSingleton<IConfirmationTokenGenerator, TestDashboardUserInviteTokenGenerator>();
            services.AddScoped<IUserRegistrationLinkProvider<OrpyxUser>, UserRegistrationLinkProviderMock<OrpyxUser>>();
            services.AddScoped<IUserRegistrationLinkProvider<Domain.DashboardUsers.DashboardUser>, UserRegistrationLinkProviderMock<Domain.DashboardUsers.DashboardUser>>();
        }
    }
}