﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Storage;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Alerts;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.CareNotes;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Devices;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.TemperatureCalculator;

namespace Orpyx.Si.Api.Pipelines.Tests.Wrappers
{
    public sealed class DoctorsServicesContainer
    {
        public PatientAlertsPipelineContainer Alerts { get; }
        public PatientsPipelineContainer Patients { get; }
        public PatientStatisticPipelineContainer PatientStatistic { get; }
        public PatientTemperatureStatisticPipelineContainer PatientTemperatureStatistic { get; }
        public CareNotesPipelineContainer CareNotes { get; }
        public IDoctorActorStorage ActorStorage { get; }
        public PatientSummaryDailyPipelineContainer SummaryDailyActorClient { get; }
        public TemperatureCalculatorContainer TemperatureCalculator { get; }
        public PatientDevicesPipelineContainer PatientDevices { get; }
        public PatientStepCountPipelineContainer PatientStepCounts { get; }
        public PatientStepCountStoragePipelineContainer PatientStepCountsStorage { get; }
        public PatientStatisticStoragePipelineContainer PatientStatisticStorage { get; }

        public DoctorsServicesContainer
        (
            IDoctorActorStorage actorStorage,
            PatientsPipelineContainer patients,
            PatientAlertsPipelineContainer alerts,
            CareNotesPipelineContainer careNotes,
            PatientSummaryDailyPipelineContainer summaryDailyActorClient,
            PatientStatisticPipelineContainer patientStatistic,
            PatientTemperatureStatisticPipelineContainer patientTemperatureStatistic,
            TemperatureCalculatorContainer temperatureCalculator,
            PatientDevicesPipelineContainer patientDevices,
            PatientStepCountPipelineContainer patientStepCounts,
            PatientStepCountStoragePipelineContainer patientStepCountsStorage,
            PatientStatisticStoragePipelineContainer patientStatisticStorage
        )
        {
            Patients = patients;
            ActorStorage = actorStorage;
            Alerts = alerts;
            CareNotes = careNotes;
            SummaryDailyActorClient = summaryDailyActorClient;
            PatientStatistic = patientStatistic;
            PatientTemperatureStatistic = patientTemperatureStatistic;
            TemperatureCalculator = temperatureCalculator;
            PatientDevices = patientDevices;
            PatientStepCounts = patientStepCounts;
            PatientStepCountsStorage = patientStepCountsStorage;
            PatientStatisticStorage = patientStatisticStorage;
        }
    }
}