﻿using Orpyx.Si.Admin.Pipelines.Common.Storage;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Organizations;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Patients;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Patients.Products;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.RemotePatientMonitoringProvider;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Users.DashboardUsers;
using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Users.OrpyxUsers;

namespace Orpyx.Si.Api.Pipelines.Tests.Wrappers
{
    public sealed class AdminServicesContainer
    {
        public PatientsPipelineContainer Patients { get; }
        public PatientProductsPipelineContainer PatientProducts { get; }
        public OrpyxUsersPipelineContainer OrpyxUsers { get; }
        public DashboardUsersPipelineContainer DashboardUsers { get; }
        public OrganizationsPipelineContainer Organizations { get; }
        public IAdminActorStorage ActorStorage { get; }
        public RemotePatientMonitoringProviderPipelineContainer RemotePatientMonitoringProviders { get; }
        public PhiLogsContainer PhiLogs { get; }

        public AdminServicesContainer
        (
            PatientsPipelineContainer patients,
            OrpyxUsersPipelineContainer orpyxUsers,
            DashboardUsersPipelineContainer dashboardUsers,
            OrganizationsPipelineContainer organizations,
            IAdminActorStorage actorStorage,
            PatientProductsPipelineContainer patientProducts,
            RemotePatientMonitoringProviderPipelineContainer remotePatientMonitoringProviders,
            PhiLogsContainer phiLogs
        )
        {
            Patients = patients;
            OrpyxUsers = orpyxUsers;
            DashboardUsers = dashboardUsers;
            Organizations = organizations;
            ActorStorage = actorStorage;
            PatientProducts = patientProducts;
            RemotePatientMonitoringProviders = remotePatientMonitoringProviders;
            PhiLogs = phiLogs;
        }
    }
}