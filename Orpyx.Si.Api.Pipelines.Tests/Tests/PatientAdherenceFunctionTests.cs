﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Dto;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Api.Pipelines.Tests.Common;
using Orpyx.Si.Business.ActionRequiredAlert;
using Orpyx.Si.Business.ActionRequiredAlert.Models;
using Orpyx.Si.Business.Patients.GetPatientsList.Filters;
using Orpyx.Si.Common;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Consts;
using Orpyx.Si.Tests.Common.Models;

namespace Orpyx.Si.Api.Pipelines.Tests.Tests.AzureFunctions.PatientAdherence
{
    internal sealed class PatientAdherenceFunctionTests : BaseTest
    {
        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        [Description("For task OSID-168: if patient vitals transmitted in required range")]
        public async Task CheckPatientAdherenceWithVitalsChangesAndWithoutClearedFlag(DateTimeWithTimeZone date)
        {
            var today = date.Today;
            var yesterday = date.Yesterday;

            var patientB2CObjectId = Guid.NewGuid();
            var organization = await Admin.Organizations.Service.Create
            (
                Admin.ActorStorage.SuperUser,
                request => request.TimeZoneOffset = date.TimeZoneOffset.Hours
            );
            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(today);

            var createdPatientId = await Doctors.Patients.Service.Create
            (
                doctor, organization.Id,
                PatientFilterType.Active,
                request => request.FirstName = "Active",
                user =>
                {
                    user.B2cobjectId = patientB2CObjectId;
                    user.AllowAccessStartDate = yesterday.AddDays(-7);
                }
            );

            await Doctors.Alerts.AlertLogReadingTable.AddAlertLogReading
            (
                doctor,
                yesterday.Date,
                patientB2CObjectId
            );

            await ProcessAfterUploadOrReset(doctor, createdPatientId, date.Yesterday);
        }

        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        [Description("For task OSID-168: if patient vitals transmitted in required range and also in the next required range")]
        public async Task CheckPatientAdherenceWithVitalsAndVitalsAtBorderDate(DateTimeWithTimeZone date)
        {
            var today = date.Today;
            var yesterday = date.Yesterday;

            var patientB2CObjectId = Guid.NewGuid();
            var organization = await Admin.Organizations.Service.Create
            (
                Admin.ActorStorage.SuperUser,
                request => request.TimeZoneOffset = date.TimeZoneOffset.Hours
            );
            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(today);

            var createdPatientId = await Doctors.Patients.Service.Create
            (
                doctor, organization.Id,
                PatientFilterType.Active,
                request => request.FirstName = "Active",
                user =>
                {
                    user.B2cobjectId = patientB2CObjectId;
                    user.AllowAccessStartDate = yesterday.AddDays(-7);
                }
            );

            await Doctors.Alerts.AlertLogReadingTable.AddAlertLogReading
            (
                doctor,
                yesterday.Date,
                patientB2CObjectId
            );

            await ProcessAfterUploadOrReset(doctor, createdPatientId, yesterday, exceptEmptyOverride: true, daysOffsetOverride: 2);

            var adherenceBorderDate = date.Tomorrow.AddDays(1);
            await TestContext.Out.WriteLineAsync($"Begin process adherence flag for border date: {adherenceBorderDate:d}");
            await Doctors.Alerts.AlertLogReadingTable.AddAlertLogReading
            (
                doctor,
                adherenceBorderDate.Date,
                patientB2CObjectId
            );

            await ProcessAfterUploadOrReset(doctor, createdPatientId, adherenceBorderDate);
        }

        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        [Description("For task OSID-168: if patient vitals transmitted, but early then required and patient has cleared alert")]
        public async Task CheckPatientAdherenceWithoutVitalsChangesAndWithClearedFlag(DateTimeWithTimeZone date)
        {
            var authorizationDate = date.Yesterday.AddDays(-2);

            var patientB2CObjectId = Guid.NewGuid();
            var organization = await Admin.Organizations.Service.Create
            (
                Admin.ActorStorage.SuperUser,
                request => request.TimeZoneOffset = date.TimeZoneOffset.Hours
            );

            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(date.Yesterday.AddDays(-1));

            var createdPatientId = await Doctors.Patients.Service.Create
            (
                doctor, organization.Id,
                PatientFilterType.Active,
                request => request.FirstName = "Active",
                user =>
                {
                    user.B2cobjectId = patientB2CObjectId;
                    user.AllowAccessStartDate = authorizationDate;
                }
            );

            await Doctors.Alerts.AlertLogReadingTable.AddAlertLogReading
            (
                doctor,
                authorizationDate.AddDays(-7).Date,
                patientB2CObjectId
            );

            {
                var actionRequiredAlerts = await ProcessAndAssertAlerts
                (
                    doctor,
                    createdPatientId,
                    date.Today,
                    exceptEmpty: false,
                    $"Begin process first adherence alert at {date.Today:d}"
                );

                var actionRequiredAlert = actionRequiredAlerts.FirstOrDefault(q => q.Type == ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days);
                Assert.IsNotNull(actionRequiredAlert, $"Failed get {nameof(ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days)}");

                doctor.ActionBeforeExecute.ClearActions();
                doctor.ActionBeforeExecute.OverrideCurrentDateAction(date.Today);
                await Doctors.CareNotes.Service.AddPatientCareNote(doctor, createdPatientId, actionRequiredAlert.Id);
            }

            {
                await ProcessAndAssertAlerts
                (
                    doctor,
                    createdPatientId,
                    date.Tomorrow,
                    exceptEmpty: true,
                    $"Begin process second adherence alert after clear alert {date.Tomorrow:d}"
                );
            }

            await ProcessAfterUploadOrReset(doctor, createdPatientId, date.Tomorrow);
        }

        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        [Description("For task OSID-168: if patient vitals transmitted, but early then required")]
        public async Task CheckPatientAdherenceWithoutVitalsChangesAndWithoutClearedFlag(DateTimeWithTimeZone date)
        {
            var today = date.Today;
            var authorizationDate = today.AddDays(-1);

            var patientB2CObjectId = Guid.NewGuid();
            var organization = await Admin.Organizations.Service.Create
            (
                Admin.ActorStorage.SuperUser,
                request => request.TimeZoneOffset = date.TimeZoneOffset.Hours
            );
            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(today);

            var createdPatientId = await Doctors.Patients.Service.Create
            (
                doctor, organization.Id,
                PatientFilterType.Active,
                request => request.FirstName = "Active",
                user =>
                {
                    user.B2cobjectId = patientB2CObjectId;
                    user.AllowAccessStartDate = authorizationDate;
                }
            );

            await Doctors.Alerts.AlertLogReadingTable.AddAlertLogReading
            (
                doctor,
                authorizationDate.AddDays(-7).Date,
                patientB2CObjectId
            );

            await ProcessAfterUploadOrReset(doctor, createdPatientId, date.Today, false);
        }

        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        [Description("For task OSID-168: if patient vitals not transmitted")]
        public async Task CheckPatientAdherenceWithoutVitalsAndWithoutClearedFlag(DateTimeWithTimeZone date)
        {
            var today = date.Today;

            var patientB2CObjectId = Guid.NewGuid();
            var organization = await Admin.Organizations.Service.Create
            (
                Admin.ActorStorage.SuperUser,
                request => request.TimeZoneOffset = date.TimeZoneOffset.Hours
            );
            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(today);

            var createdPatientId = await Doctors.Patients.Service.Create
            (
                doctor, organization.Id,
                PatientFilterType.Active,
                request => request.FirstName = "Active",
                user =>
                {
                    user.B2cobjectId = patientB2CObjectId;
                    user.AllowAccessStartDate = today.AddDays(-3);
                }
            );

            await ProcessAfterUploadOrReset(doctor, createdPatientId, date.Today, false);
        }

        private async Task<IReadOnlyList<ActionRequiredAlertsEntry>> ProcessAndAssertAlerts(IDoctorActorExecutor doctor, int patientId, DateTimeOffset today, bool exceptEmpty, string message)
        {
            await TestContext.Out.WriteLineAsync(message);

            doctor.ActionBeforeExecute.ClearActions();
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(today);
            await AzureFunctions.PatientAdherence.Service.ProcessPatients(doctor, patientId);

            var alertsAfterProcess = await Doctors.Alerts.Service.GetActionRequiredAlerts(doctor, patientId);
            AssertIsEmptyOrDataReviewAlerts(alertsAfterProcess, exceptEmpty);
            return alertsAfterProcess.Entries;
        }

        private async Task ProcessAfterUploadOrReset(IDoctorActorExecutor doctor, int patientId, DateTimeOffset uploadOrResetDate, bool? exceptEmptyOverride = null, int? daysOffsetOverride = null)
        {
            var maxDaysOffset = daysOffsetOverride ?? 4;
            for (var dayOffset = 1; dayOffset <= maxDaysOffset; dayOffset++)
            {
                var dateToCheck = uploadOrResetDate.AddDays(dayOffset);
                var exceptEmpty = exceptEmptyOverride ?? dayOffset < 3;

                await ProcessAndAssertAlerts
                (
                    doctor,
                    patientId,
                    dateToCheck,
                    exceptEmpty,
                    $"Begin check adherence {dateToCheck:d} | {(dateToCheck - uploadOrResetDate).Days} days after uploadOrResetDate {uploadOrResetDate:d} | exceptEmpty: {exceptEmpty}"
                );
            }
        }

        void AssertIsEmptyOrDataReviewAlerts(ListActionRequiredAlertsResult alerts, bool exceptEmpty)
        {
            if (exceptEmpty)
            {
                Assert.IsTrue(alerts.Entries.IsEmpty() || alerts.Entries.All(a => a.Type == ActionRequiredAlertTypeEnum.DataReviewWith7Days), "Patient shouldn't have alerts or should have only data review alerts");
                Assert.LessOrEqual(alerts.Entries.Count, 1, "Patient should have optional data review alert");
            }
            else
            {
                Assert.LessOrEqual(alerts.Entries.Count, 2, "Patient should have adherence alerts and optional data review");

                Assert.IsTrue
                (
                    alerts.Entries.All(a => a.Type == ActionRequiredAlertTypeEnum.DataReviewWith7Days || a.Type == ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days),
                    "Patient should have adherence alerts and optional data review"
                );
            }
        }
    }
}