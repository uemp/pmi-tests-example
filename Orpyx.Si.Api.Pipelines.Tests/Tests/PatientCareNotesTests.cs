﻿using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Tests.Common.Consts;
using Orpyx.Si.Api.Pipelines.Tests.Common;
using Orpyx.Si.Business.Patients.GetPatientsList;
using Orpyx.Si.Business.Patients.GetPatientsList.Filters;
using Orpyx.Si.Tests.Common.Models;

namespace Orpyx.Si.Api.Pipelines.Tests.Tests.Patients.CareNotes
{
    internal sealed class PatientCareNotesTests : BaseTest
    {
        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DefaultDateWithTimeZone))]
        public async Task DaysSinceLastReviewAfterAddCareNote(DateTimeWithTimeZone data)
        {
            var organization = await Admin.Organizations.Service.Create(Admin.ActorStorage.SuperUser, data.TimeZoneOffset);
            
            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(data.Today);

            var createdPatientId = await Doctors.Patients.Service.Create(doctor,  organization.Id, PatientFilterType.Active, request => request.FirstName = "Active", user =>
            {
                user.AllowAccessStartDate = data.Yesterday;
            });
            var patientsBeforeAddCareNote = await Doctors.Patients.Service.GetPatientsList(doctor, organization.Id, PatientFilterType.Active);

            Assert.IsNotEmpty(patientsBeforeAddCareNote.Items, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active})");
            Assert.IsNull(patientsBeforeAddCareNote.Items[0].DaysSinceLastReview, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active}) | Invalid {nameof(GetPatientsListResponse.DaysSinceLastReview)}");
            
            await Doctors.CareNotes.Service.AddPatientCareNote(doctor, createdPatientId, request => request.CarePractitionerId = doctor.Actor.B2CObjectId);
            var patientsAfterAddCareNote = await Doctors.Patients.Service.GetPatientsList(doctor, organization.Id, PatientFilterType.Active);

            Assert.IsNotEmpty(patientsAfterAddCareNote.Items, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active})");
            Assert.IsNotNull(patientsAfterAddCareNote.Items[0].DaysSinceLastReview, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active}) | Invalid {nameof(GetPatientsListResponse.DaysSinceLastReview)}");
            Assert.AreEqual(patientsAfterAddCareNote.Items[0].DaysSinceLastReview, 0, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active}) | Invalid {nameof(GetPatientsListResponse.DaysSinceLastReview)}");

            var reviewDate = data.Today.AddDays(3);
            var daysBetweenSubmitCareNoteAndReviewDate = (reviewDate - data.Today).Days;
            
            doctor.ActionBeforeExecute.OverrideCurrentDateAction(reviewDate);
            var patientsAfterAddCareNoteAfterTime = await Doctors.Patients.Service.GetPatientsList(doctor, organization.Id, PatientFilterType.Active);

            Assert.IsNotEmpty(patientsAfterAddCareNoteAfterTime.Items, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active})");
            Assert.IsNotNull(patientsAfterAddCareNoteAfterTime.Items[0].DaysSinceLastReview, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active}) | Invalid {nameof(GetPatientsListResponse.DaysSinceLastReview)}");
            Assert.AreEqual(patientsAfterAddCareNoteAfterTime.Items[0].DaysSinceLastReview, daysBetweenSubmitCareNoteAndReviewDate, $"Failed GetPatientsList({organization.Id}, {PatientFilterType.Active}) | Invalid {nameof(GetPatientsListResponse.DaysSinceLastReview)}");
        }
    }
}