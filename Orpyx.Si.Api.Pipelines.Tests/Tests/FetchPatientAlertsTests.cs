﻿using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Asserts;
using Orpyx.Si.Tests.Common.Consts;
using Orpyx.Si.Api.Pipelines.Common.Dto;
using Orpyx.Si.Api.Pipelines.Tests.Common;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Models;

namespace Orpyx.Si.Api.Pipelines.Tests.Tests.Patients.Alerts
{
    internal sealed class FetchPatientAlertsTests : BaseTest
    {
        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        public async Task CheckPatientAlertsWithAlerts(DateTimeWithTimeZone data)
        {
            var organization = await Admin.Organizations.Service.Create(Admin.ActorStorage.SuperUser, data.TimeZoneOffset);

            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            var (patientId, b2CObjectId) = await Doctors.Patients.Service.CreateActive(doctor, data.Today.AddDays(-7));

            await Doctors.Alerts.Service.CreateActionRequiredAlert(doctor, new CreateActionRequiredAlertRequest
            {
                PatientId = patientId,
                AlertTypeId = Doctors.Alerts.Storage.GetBy(ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days).Id,
                Date = data.Today
            });

            var alerts = await Doctors.Alerts.Service.GetActionRequiredAlerts(doctor, patientId);
            alerts.AssertAnyOfAlert(ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days);
        }   
        
        [Test]
        [TestCaseSource(typeof(DateTimeConsts), nameof(DateTimeConsts.DateWithTimeZone))]
        public async Task CheckPatientAlertsWithoutAlerts(DateTimeWithTimeZone data)
        {
            var organization = await Admin.Organizations.Service.Create(Admin.ActorStorage.SuperUser, data.TimeZoneOffset);

            var doctor = await Doctors.ActorStorage.CreateActorWith(organization.Id);
            var (patientId, b2CObjectId) = await Doctors.Patients.Service.CreateActive(doctor, data.Today.AddDays(-7));
            
            var alerts = await Doctors.Alerts.Service.GetActionRequiredAlerts(doctor, patientId);
            alerts.AssertEmptyOrReviewAlert();
        }
    }
}