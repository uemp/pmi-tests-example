﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Business.ActionRequiredAlert.Services.Alerts;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Storage.ActionRequiredAlertTypes
{
    public sealed class ActionRequiredAlertTypesStorage : IPipelineStorage
    {
        public ActionRequiredAlertType[] AlertTypes { get; private set; }

        public async Task Setup(IBaseActorExecutor executor)
        {
            AlertTypes = await executor.ExecuteInScope(provider =>
            {
                var service = provider.GetRequiredService<IActionRequiredAlertService>();
                return service.GetTypes();
            });
            
            AssertAlertTypesInitialization();
        }

        public ActionRequiredAlertType GetBy(ActionRequiredAlertTypeEnum type)
        {
            AssertAlertTypesInitialization();
            return AlertTypes.First(q => q.Type == type);
        }
        
        public ActionRequiredAlertType GetBy(int id)
        {
            AssertAlertTypesInitialization();
            return AlertTypes.First(q => q.Id == id);
        }

        private void AssertAlertTypesInitialization()
        {
            Assert.IsNotNull(AlertTypes, $"AlertTypes not initialized");
            Assert.IsNotEmpty(AlertTypes, $"AlertTypes not initialized");
        }
    }
}