﻿using System.Threading.Tasks;
using Orpyx.Si.Admin;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Domain.DashboardUsers;

namespace Orpyx.Si.Api.Pipelines.Common.Storage
{
    public interface IDoctorActorStorage
    {
        Task<IDoctorActorExecutor> CreateActorFor(UserOutputDto dashboardUser);
        Task<IDoctorActorExecutor> CreateActorWith(int organizationId, DashboardUserTitleType title = DashboardUserTitleType.Other);
    }
}