﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Admin;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Providers;
using Orpyx.Si.Domain.DashboardUsers;

namespace Orpyx.Si.Api.Pipelines.Common.Storage
{
    internal sealed class DoctorActorStorage : IDoctorActorStorage
    {
        private readonly IServiceProvider serviceProvider;

        public DoctorActorStorage(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task<IDoctorActorExecutor> CreateActorFor(UserOutputDto dashboardUser)
        {
            using var scopedServiceProvider = serviceProvider.CreateScope();
            var actorProvider = scopedServiceProvider.ServiceProvider.GetRequiredService<DashboardUserActorProvider>();
            return await actorProvider.Provide(serviceProvider, dashboardUser);
        }

        public async Task<IDoctorActorExecutor> CreateActorWith(int organizationId, DashboardUserTitleType title = DashboardUserTitleType.Other)
        {
            using var scopedServiceProvider = serviceProvider.CreateScope();
            var actorProvider = scopedServiceProvider.ServiceProvider.GetRequiredService<DashboardUserActorProvider>();
            return await actorProvider.Provide(serviceProvider, organizationId, title);
        }
    }
}