﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.DbQueries;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.CareNotes;
using Orpyx.Si.Api.Pipelines.Common.Providers;
using Orpyx.Si.Api.Pipelines.Common.Storage;
using Orpyx.Si.Business.Actors.Provider;
using Orpyx.Si.Business.CareNote.SubmitCareNote;
using Orpyx.Si.Domain.DashboardUsers;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Extensions
{
    public static class IntegrationTestsExtensions
    {
        /// <summary>
        /// Add this resolver to end of DI initialization for correct override
        /// </summary>
        public static IServiceCollection AddCarePractitionerIdentifierResolver(this IServiceCollection services)
        {
            services.AddScoped<TestCarePractitionerIdentifierResolver>();
            services.AddScoped<IDashboardUserActorProvider, TestCarePractitionerIdentifierResolver>(provider => provider.GetRequiredService<TestCarePractitionerIdentifierResolver>());
            return services;
        }
        
        public static IServiceCollection AddDoctorsPipelinesServices(this IServiceCollection services)
        {
            services.AddPipelineServices(typeof(IntegrationTestsExtensions).Assembly);
            
            services.AddSingleton<PatientsActorDbQueries>();
            services.AddSingleton<DoctorActorStorage>();
            services.AddSingleton<IDoctorActorStorage, DoctorActorStorage>(provider => provider.GetRequiredService<DoctorActorStorage>());

            services.AddScoped<DashboardUserActorProvider>();
            
            return services;
        }

        public static void UseDoctorsActorExecutorBuilder(this IApplicationBuilder builder)
        {
            
        }
    }
}