﻿using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Business.Patients.Statistics.Temperature.Dto;
using Orpyx.Si.Common.Dates;

namespace Orpyx.Si.Api.Pipelines.Common.Extensions.Patient
{
    public static class PatientTemperatureExtensions
    {
        public static async Task Dump(this PatientTemperatureSummaryModel model, DateTimeRange range)
        {
            await TestContext.Out.WriteLineAsync($"Temperature for range: {range} | Days: {range.GetDays(includeEndDate: false).Count()} | Rows: {model.DailySummary.Length}");

            foreach (var summary in model.DailySummary)
            {
                await TestContext.Out.WriteLineAsync($"Temperature[{summary.DateTimeToDay}] | Left: [{summary.LeftFoot}] | Right: [{summary.RightFoot}]");
            }
        }
    }
}