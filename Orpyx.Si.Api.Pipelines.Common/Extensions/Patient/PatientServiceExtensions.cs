﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Common.Azure.Functions.Patients;
using Orpyx.Si.Common.Azure.Functions.Patients.Dto;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain.Patients.Extensions;
using Orpyx.Si.Domain.Patients.Models;
using Orpyx.Si.Tests.Common.Actors;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Extensions.Patient
{
    public static class PatientServiceExtensions
    {
        public static Task<PatientDto> GetPatientById(this IBaseActorExecutor actor, int patientId)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();
                var patient = await dbContext.Patient.GetDtoByIdAsync(patientId);
                Assert.NotNull(patient, $"Failed {nameof(GetPatientById)}({patientId})");
                return patient;
            });
        }     
        
        public static Task<AzurePatientDto> GetAzurePatientById(this IBaseActorExecutor actor, int patientId)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var patientService = provider.GetRequiredService<IAzurePatientService>();
                var patient = await patientService.GetActivePatient(patientId);
                patient.AssertIsSucceeded($"Failed {nameof(GetPatientById)}({patientId})");
                return patient.Result;
            });
        }
    }
}