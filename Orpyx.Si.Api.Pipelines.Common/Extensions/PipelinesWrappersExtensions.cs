﻿using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Alerts;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.CareNotes;
using Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients;

namespace Orpyx.Si.Api.Pipelines.Common.Extensions
{
    internal static class PipelinesWrappersExtensions
    {
        public static IServiceCollection AddPipelinesWrappers(this IServiceCollection services)
        {
            services.AddSingleton<PatientAlertsPipelineContainer>();
            
            services.AddSingleton<CareNotesPipelineGenerator>();
            services.AddSingleton<CareNotesPipelineContainer>();
            
            services.AddSingleton<PatientsPipelineContainer>();

            return services;
        }
    }
}