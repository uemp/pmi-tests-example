﻿using System;
using System.Threading.Tasks;
using Orpyx.Si.Business.Actors;
using Orpyx.Si.Business.Actors.Provider;
using Orpyx.Si.Common.Operations;

namespace Orpyx.Si.Api.Pipelines.Common.Executors
{
    internal sealed class TestCarePractitionerIdentifierResolver : IDashboardUserActorProvider
    {
        public DashboardUserActor Actor { get; set; }

        public Task<OperationResult<DashboardUserActor>> GetDashboardUserActor()
        {
            return Task.FromResult(new OperationResult<DashboardUserActor>(Actor));
        }
    }
}