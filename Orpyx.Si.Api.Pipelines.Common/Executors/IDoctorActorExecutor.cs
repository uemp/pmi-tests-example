﻿using Orpyx.Si.Business.Actors;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Executors
{
    public interface IDoctorActorExecutor: IBaseActorExecutor<DashboardUserActor>
    {
        
    }
}