﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Business.Actors;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Executors
{
    internal sealed class DoctorActorExecutor : BaseActorExecutor<DashboardUserActor>, IDoctorActorExecutor
    {
        public DoctorActorExecutor(IServiceProvider serviceProvider, DashboardUserActor actor) : base(serviceProvider, actor)
        {
            
        }

        protected override void ConfigureServiceProvider(IServiceProvider serviceProvider)
        {
            serviceProvider.GetRequiredService<TestCarePractitionerIdentifierResolver>().Actor = Actor;
        }
    }
}