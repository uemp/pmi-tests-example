﻿using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.CareNotes;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.CareNotes
{
    public sealed class CareNotesPipelineGenerator : IPipelineGenerator
    {
        public CreateCareNoteRequestGenerator Create { get; }

        public CareNotesPipelineGenerator(CreateCareNoteRequestGenerator create)
        {
            Create = create;
        }
    }
}