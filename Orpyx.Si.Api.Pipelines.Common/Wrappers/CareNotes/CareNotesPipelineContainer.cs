﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.CareNotes
{
    public class CareNotesPipelineContainer : PipelineServiceContainer<PatientCareNotesActorClient, PatientCareNotesActorService, CareNotesPipelineGenerator>
    {
        public CareNotesPipelineContainer(PatientCareNotesActorClient client, PatientCareNotesActorService service, CareNotesPipelineGenerator generators) : base(client, service, generators)
        {
        }
    }
}