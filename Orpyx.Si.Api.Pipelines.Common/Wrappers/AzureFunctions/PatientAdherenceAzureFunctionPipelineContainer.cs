﻿using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.AzureFunctions
{
    public sealed class PatientAdherenceAzureFunctionPipelineContainer : PipelineServiceContainer<PatientAdherenceFunctionActorClient, PatientAdherenceFunctionActorClient>
    {
        public PatientAdherenceAzureFunctionPipelineContainer(PatientAdherenceFunctionActorClient client, PatientAdherenceFunctionActorClient service) : base(client, service)
        {
        }
    }
}