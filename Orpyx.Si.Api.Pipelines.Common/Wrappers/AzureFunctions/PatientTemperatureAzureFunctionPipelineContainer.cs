﻿using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Api.Pipelines.Common.Services.AzureFunctions;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.AzureFunctions
{
    public class PatientTemperatureAzureFunctionPipelineContainer : PipelineServiceContainer<PatientTemperatureAzureFunctionActorClient, PatientTemperatureAzureFunctionActorService>
    {
        public PatientTemperatureAzureFunctionPipelineContainer(PatientTemperatureAzureFunctionActorClient client, PatientTemperatureAzureFunctionActorService service) : base(client, service)
        {
        }
    }
}