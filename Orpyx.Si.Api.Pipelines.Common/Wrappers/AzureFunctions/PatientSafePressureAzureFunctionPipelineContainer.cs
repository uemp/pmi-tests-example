﻿using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Api.Pipelines.Common.Services.AzureFunctions;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.AzureFunctions
{
    public sealed class PatientSafePressureAzureFunctionPipelineContainer : PipelineServiceContainer<PatientSafePressureAzureFunctionActorClient, PatientSafePressureAzureFunctionActorService>
    {
        public PatientSafePressureAzureFunctionPipelineContainer(PatientSafePressureAzureFunctionActorClient client, PatientSafePressureAzureFunctionActorService service) : base(client, service)
        {
        }
    }
}