﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Api.Pipelines.Common.Services.TemperatureCalculator;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.TemperatureCalculator
{
    public class TemperatureCalculatorContainer : PipelineServiceContainer<TemperatureCalculatorActorClient, TemperatureCalculatorActorService, TemperatureCalculatorGenerator>
    {
        public TemperatureCalculatorContainer(TemperatureCalculatorActorClient client, TemperatureCalculatorActorService service, TemperatureCalculatorGenerator generators) : base(client, service, generators)
        {
        }
    }
}