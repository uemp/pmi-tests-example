﻿using Orpyx.Si.Api.Pipelines.Common.Generators.TemperatureCalculator;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.TemperatureCalculator
{
    public class TemperatureCalculatorGenerator : IPipelineGenerator
    {
        public PlantarTemperatureLogGenerator Create { get; }

        public TemperatureCalculatorGenerator(PlantarTemperatureLogGenerator create)
        {
            Create = create;
        }

    }
}