﻿using Orpyx.Si.Admin.Pipelines.Common.Wrappers.Patients;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.DbQueries;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;
namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients
{
    public sealed class PatientsPipelineContainer : PipelineServiceContainer<PatientsActorClient, PatientsActorService, PatientsPipelineGenerators>
    {
        public PatientsActorDbQueries DbQueries { get; }
        public PatientsPipelineContainer(PatientsActorClient client, PatientsActorService service, PatientsPipelineGenerators generators, PatientsActorDbQueries dbQueries) : base(client, service, generators)
        {
            DbQueries = dbQueries;
        }
    }
}