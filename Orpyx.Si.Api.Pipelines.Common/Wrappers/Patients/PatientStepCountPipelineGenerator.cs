﻿using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.StepCounts;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients
{
    public sealed class PatientStepCountPipelineGenerator : IPipelineGenerator
    {
        public CreateDailyPatientStepCountsGenerator Create { get; }

        public PatientStepCountPipelineGenerator(CreateDailyPatientStepCountsGenerator create)
        {
            Create = create;
        }
    }
}