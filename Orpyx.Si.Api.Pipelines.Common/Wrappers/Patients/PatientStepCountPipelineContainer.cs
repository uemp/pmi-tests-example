﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients
{
    public sealed class PatientStepCountPipelineContainer : PipelineServiceContainer<PatientStepCountActorClient, PatientStepCountActorService, PatientStepCountPipelineGenerator>
    {
        public PatientStepCountPipelineContainer(PatientStepCountActorClient client, PatientStepCountActorService service, PatientStepCountPipelineGenerator generators) : base(client, service, generators)
        {
        }
    }
}