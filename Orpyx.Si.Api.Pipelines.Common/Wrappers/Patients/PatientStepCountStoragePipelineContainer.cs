﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients
{
    public sealed class PatientStepCountStoragePipelineContainer : PipelineServiceContainer<PatientStepCountStorageActorClient, PatientStepCountStorageActorService>
    {
        public PatientStepCountStoragePipelineContainer(PatientStepCountStorageActorClient client, PatientStepCountStorageActorService service) : base(client, service)
        {
        }
    }
}