﻿using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Devices;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Devices
{
    public class PatientDevicesPipelineGenerator : IPipelineGenerator
    {
        public PatientDevicesPipelineGenerator(CreatePatientDeviceRequestGenerator create)
        {
            Create = create;
        }

        public CreatePatientDeviceRequestGenerator Create { get; }
    }
}