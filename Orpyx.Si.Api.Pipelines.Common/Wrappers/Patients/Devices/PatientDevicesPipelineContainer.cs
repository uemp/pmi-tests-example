﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Devices
{
    public class PatientDevicesPipelineContainer : PipelineServiceContainer<PatientDevicesActorClient, PatientDevicesActorService, PatientDevicesPipelineGenerator>
    {
        public PatientDevicesPipelineContainer(PatientDevicesActorClient client, PatientDevicesActorService service, PatientDevicesPipelineGenerator generators) : base(client, service, generators)
        {
        }
    }
}