﻿using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics
{
    public sealed class PatientStatisticPipelineContainer : PipelineServiceContainer<PatientStatisticActorClient, PatientStatisticActorService>
    {
        public PatientStatisticPipelineContainer(PatientStatisticActorClient client, PatientStatisticActorService service) : base(client, service)
        {
        }
    }
}