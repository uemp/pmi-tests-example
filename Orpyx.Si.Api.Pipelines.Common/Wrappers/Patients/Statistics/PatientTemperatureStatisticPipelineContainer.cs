﻿using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics
{
    public sealed class PatientTemperatureStatisticPipelineContainer : PipelineServiceContainer<PatientTemperatureStatisticActorClient, PatientTemperatureStatisticActorService, PatientTemperatureStatisticPipelineGenerator>
    {
        public PatientTemperatureStatisticPipelineContainer(PatientTemperatureStatisticActorClient client, PatientTemperatureStatisticActorService service, PatientTemperatureStatisticPipelineGenerator generators) : base(client, service, generators)
        {
        }
    }
}