﻿using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Generators.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Services.Statistics;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics
{
    public class PatientSummaryDailyPipelineContainer : PipelineServiceContainer<PatientSummaryDailyActorClient, PatientSummaryDailyActorService, PatientSummaryDailyGenerator>
    {
        public PatientSummaryDailyPipelineContainer(PatientSummaryDailyActorClient client, PatientSummaryDailyActorService service, PatientSummaryDailyGenerator generators) : base(client, service, generators)
        {
        }
    }
}