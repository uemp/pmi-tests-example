﻿using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Statistics;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics
{
    public sealed class PatientTemperatureStatisticPipelineGenerator : IPipelineGenerator
    {      
        public AddPatientTemperatureStatisticRequestGenerator Create { get; }

        public PatientTemperatureStatisticPipelineGenerator(AddPatientTemperatureStatisticRequestGenerator create)
        {
            Create = create;
        }

    }
}