﻿using IngestorProcessor.Common.Tests.Common.Generators;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Services.Statistics;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Patients.Statistics
{
    public sealed class PatientStatisticStoragePipelineContainer : PipelineServiceContainer<PatientStatisticStorageActorClient, PatientStatisticStorageActorService, PatientSummaryStatisticGenerator>
    {
        public PatientStatisticStoragePipelineContainer(PatientStatisticStorageActorClient client, PatientStatisticStorageActorService service, PatientSummaryStatisticGenerator generators) : base(client, service, generators)
        {
        }
    }
}