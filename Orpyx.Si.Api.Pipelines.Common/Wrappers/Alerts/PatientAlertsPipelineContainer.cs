﻿using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Services.Patients;
using Orpyx.Si.Api.Pipelines.Common.Storage.ActionRequiredAlertTypes;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Wrappers.Alerts
{
    public class PatientAlertsPipelineContainer : PipelineServiceContainer<PatientAlertsActorClient, PatientAlertsActorService>
    {
        public AlertLogReadingTableClient AlertLogReadingTable { get; }
        public ActionRequiredAlertTypesStorage Storage { get; }

        public PatientAlertsPipelineContainer
        (
            PatientAlertsActorClient client,
            PatientAlertsActorService service,
            AlertLogReadingTableClient alertLogReadingTable,
            ActionRequiredAlertTypesStorage storage
        ) : base(client, service)
        {
            AlertLogReadingTable = alertLogReadingTable;
            Storage = storage;
        }
    }
}