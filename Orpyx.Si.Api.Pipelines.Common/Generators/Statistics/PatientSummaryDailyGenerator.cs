﻿using System;
using System.Linq;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Storage;
using Orpyx.Si.Storage.UserAlerts.Data;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Statistics
{
    public class PatientSummaryDailyGenerator : IPipelineGenerator
    {
        public InsertSideDaily CreateInsertSummaryDaily(DateTime date, int side, int usageDurationCoefficient = 1000, int alertDurationCoefficient = 1000, int alertsCoefficient = 1000)
        {
            return new InsertSideDaily(date, side)
            {
                Alerts = CreateHoursSequence(alertsCoefficient != 0 ? 2400 : alertsCoefficient),

                //    AlertDurationSecs
                AlertDuration = CreateHoursSequence(alertDurationCoefficient),

                //    ActiveDurationSecs
                UsageDuration = CreateHoursSequence(usageDurationCoefficient),

                AlertsHeel = CreateHoursSequence(alertsCoefficient),
                AlertsMidfoot = CreateHoursSequence(alertsCoefficient),
                AlertsInnerMetas = CreateHoursSequence(alertsCoefficient),
                AlertsInnerToes = CreateHoursSequence(alertsCoefficient),
                AlertsOuterMetas = CreateHoursSequence(alertsCoefficient),
                AlertsOuterToes = CreateHoursSequence(alertsCoefficient),

                AlertDurationHeel = CreateHoursSequence(alertDurationCoefficient),
                AlertDurationMidfoot = CreateHoursSequence(alertDurationCoefficient),

                AlertDurationInnerMetas = CreateHoursSequence(alertDurationCoefficient),
                AlertDurationInnerToes = CreateHoursSequence(alertDurationCoefficient),
                AlertDurationOuterMetas = CreateHoursSequence(alertDurationCoefficient),
                AlertDurationOuterToes = CreateHoursSequence(alertDurationCoefficient),

                InsertId = Guid.NewGuid().ToString()
            };
        }
        
        public SummaryDaily CreateSummaryDaily
        (
            DateTime date,
            int side,
            int usageDurationCoefficient = 1000,
            int alertDurationCoefficient = 1000,
            int alertsCoefficient = 1000,
            Action<ushort[]> customizeAlerts = default,
            Action<ushort[]> customizeAlertDuration = default,
            Action<ushort[]> customizeUsageDuration = default
        )
        {
            //  TODO: Need pass date with inversed Time zone offset for correct calculate.
            //  If timezone -7, need add 7 hours.
            return new SummaryDaily(date)
            {
                Alerts = CreateHoursSequence(alertsCoefficient == 0 ? 2400 : alertsCoefficient, customizeAlerts),

                //    AlertDurationSecs
                AlertDuration = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                //    ActiveDurationSecs
                UsageDuration = CreateHoursSequence(usageDurationCoefficient, customizeUsageDuration),

                AlertsHeel = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsMidfoot = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsInnerMetas = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsInnerToes = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsOuterMetas = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsOuterToes = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),

                AlertDurationHeel = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationMidfoot = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                AlertDurationInnerMetas = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationInnerToes = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationOuterMetas = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationOuterToes = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                InsertSide = side
            };
        }

        public InsertSideDaily CreateInsertSideDaily
        (
            DateTime date,
            int side,
            int usageDurationCoefficient = 1000,
            int alertDurationCoefficient = 1000,
            int alertsCoefficient = 1000,
            Action<ushort[]> customizeAlerts = default,
            Action<ushort[]> customizeAlertDuration = default,
            Action<ushort[]> customizeUsageDuration = default
        )
        {
            //  TODO: Need pass date with inversed Time zone offset for correct calculate.
            //  If timezone -7, need add 7 hours.
            return new InsertSideDaily(date, side)
            {
                Alerts = CreateHoursSequence(alertsCoefficient != 0 ? 2400 : alertsCoefficient, customizeAlerts),

                //    AlertDurationSecs
                AlertDuration = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                //    ActiveDurationSecs
                UsageDuration = CreateHoursSequence(usageDurationCoefficient, customizeUsageDuration),

                AlertsHeel = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsMidfoot = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsInnerMetas = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsInnerToes = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsOuterMetas = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),
                AlertsOuterToes = CreateHoursSequence(alertsCoefficient, customizeUsageDuration),

                AlertDurationHeel = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationMidfoot = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                AlertDurationInnerMetas = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationInnerToes = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationOuterMetas = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),
                AlertDurationOuterToes = CreateHoursSequence(alertDurationCoefficient, customizeAlertDuration),

                InsertSide = side
            };
        }

        public byte[] CreateHoursSequence(int coefficient, Action<ushort[]> customize = default)
        {
            var values = Enumerable.Range(0, TimeConsts.HoursInDay)
                .Select(hour => (UInt16) ( /*(hour + 1) **/ coefficient))
                .ToArray();

            customize?.Invoke(values);

            var bytes = TableUtilities.Encode(values);
            return bytes;
        }

        public SummaryDaily GenerateSample(DateTime generatedAt, int[] hoursWithChanges)
        {
            var summaryDaily = CreateSummaryDaily
            (
                generatedAt,
                1,
                1000,
                40,
                100,
                alerts => ResetIfNotExist(alerts, hoursWithChanges),
                alertsDuration => ResetIfNotExist(alertsDuration, hoursWithChanges),
                usageDuration => ResetIfNotExist(usageDuration, hoursWithChanges)
            );

            return summaryDaily;
        }

        public InsertSideDaily GenerateInsertSample(DateTime generatedAt, int[] hoursWithChanges)
        {
            var summaryDaily = CreateInsertSideDaily
            (
                generatedAt,
                1,
                1000,
                40,
                100,
                alerts => ResetIfNotExist(alerts, hoursWithChanges),
                alertsDuration => ResetIfNotExist(alertsDuration, hoursWithChanges),
                usageDuration => ResetIfNotExist(usageDuration, hoursWithChanges)
            );

            return summaryDaily;
        }

        public static void ResetIfNotExist(ushort[] values, params int[] hoursWithChanges)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (hoursWithChanges.All(hour => hour != i))
                {
                    values[i] = 0;
                }
            }
        }
    }
}