﻿using System;
using Orpyx.Si.Storage.StepCounts.Data;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Patients.StepCounts
{
    public class CreateDailyPatientStepCountsGenerator : IPipelineGenerator
    {
        public const int MinimumSteps = 0;
        public const int MaximumSteps = 50000;

        public static int RandomSteps => DataGenerator.Random.Next(MinimumSteps, MaximumSteps);
        
        public DailyPatientStepCounts Generate(int patientId, DateTime date, Action<DailyPatientStepCounts> customize = default, int? minSteps = default, int? maxSteps = default)
        {
            var steps = new DailyPatientStepCounts(patientId, date)
            {
                Count = DataGenerator.Random.Next(minSteps ?? MinimumSteps, maxSteps ?? MaximumSteps),
            };

            customize?.Invoke(steps);
            return steps;
        }

        public DailyPatientStepCounts Generate(int patientId, DateTime date, int stepsCount, Action<DailyPatientStepCounts> customize = default)
        {
            var steps = new DailyPatientStepCounts(patientId, date)
            {
                Count = stepsCount,
            };

            customize?.Invoke(steps);
            return steps;
        }
    }
}