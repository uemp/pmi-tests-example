﻿using System;
using System.Collections.Generic;
using Orpyx.Si.Business.PatientDevices;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Devices
{
    public class CreatePatientDeviceRequestGenerator : IPipelineGenerator
    {
        public SubmitDeviceData Generate(Guid patientB2CObjectId, IReadOnlyList<uint> deviceIds, Action<SubmitDeviceData> customize = default)
        {
            var request = new SubmitDeviceData
            {
                NameForDevice = DataGenerator.RandomString(8),
                MobileUserId = patientB2CObjectId,
                Devices = deviceIds
            };
            
            customize?.Invoke(request);
            return request;
        }
        
        public SubmitDeviceData Generate(Guid patientB2CObjectId, Action<SubmitDeviceData> customize = default)
        {
            var request = new SubmitDeviceData
            {
                NameForDevice = DataGenerator.RandomString(8),
                MobileUserId = patientB2CObjectId,
            };
            
            customize?.Invoke(request);
            return request;
        }
        
    }
}