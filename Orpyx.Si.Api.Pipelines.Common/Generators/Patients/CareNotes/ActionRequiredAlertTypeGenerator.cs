﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orpyx.Si.Common;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Patients.CareNotes
{
    public static class ActionRequiredAlertTypeGenerator
    {
        public static ActionRequiredAlertTypeEnum[] Types { get; } = EnumerableExtension.GetEnumValues<ActionRequiredAlertTypeEnum>();

        public static ActionRequiredAlertTypeEnum[][] Except(ActionRequiredAlertTypeEnum except)
        {
            return With(q => !q.Contains(except));
        }

        public static ActionRequiredAlertTypeEnum[][] With(Func<IEnumerable<ActionRequiredAlertTypeEnum>, bool> predicate)
        {
            var combinations = EnumerableExtension.GetAllCombinations(Types, Types.Length)
                .Where(predicate)
                .Select(q => q.Distinct().OrderBy(w => w).ToArray())
                .Distinct(EnumArrayEqualityComparer<ActionRequiredAlertTypeEnum>.Comparer)
                .ToArray();

            return combinations;
        }
    }
}