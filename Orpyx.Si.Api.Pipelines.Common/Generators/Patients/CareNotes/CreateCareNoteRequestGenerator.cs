﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Business.CareNote.SubmitCareNote;
using Orpyx.Si.Business.Report.ActivityLogReport;
using Orpyx.Si.Business.Report.ActivityLogReport.Dto;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain.Patients.Extensions;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Patients.CareNotes
{
    public sealed class CreateCareNoteRequestGenerator : IPipelineGenerator
    {
        public async Task<SubmitCareNoteRequest> Generate(IDoctorActorExecutor actor, int patientId, Action<SubmitCareNoteRequest> customize)
        {
            var patient = await actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();
                return await dbContext.Patient.GetDtoByIdAsync(patientId);
            });
            
            var request = new SubmitCareNoteRequest
            {
                Patient = patient,

                NoteText = DataGenerator.RandomString(32),
                TrackingTime = TimeSpan.FromSeconds(10),
                ActionRequiredAlertIds = Array.Empty<int>(),
                ActivityLogReportBuildingOptions = new ActivityLogReportOptions()
                {
                    Temperature = new TemperatureOptions()
                    {
                        Foot = Foot.LeftFoot,
                        Sensor = Sensor.Heel,
                        ComparedTo = ComparedTo.Contralateral
                    },
                },
                //    TODO: Use from store
                CarePractitionerActionTypeId = 1,
            };

            customize?.Invoke(request);
            return request;
        }
    }
}