﻿using System;
using Orpyx.Si.Api.Pipelines.Common.Dto;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Storage.PatientStatisticTemperature;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Statistics
{
    [Flags]
    public enum PatientTemperatureStatisticGeneratorOptions
    {
        GenerateRandom = 1,
        GenerateAlwaysRight = 2,
        GenerateAlwaysWrong = 4,

        GenerateForLeftInsole = 8,
        GenerateForRightInsole = 16,
    }

    public sealed class AddPatientTemperatureStatisticRequestGenerator : IPipelineGenerator
    {
        public AddPatientTemperatureStatisticRequest Generate
        (
            int patientId,
            DateTime day,
            PatientTemperatureStatisticGeneratorOptions options,
            Action<AddPatientTemperatureStatisticRequest> customize = default
        )
        {
            var request = new AddPatientTemperatureStatisticRequest
            {
                PatientId = patientId,
                Day = day,
                LeftFoot = GenerateFootStats(options, isLeft: true),
                RightFoot = GenerateFootStats(options, isLeft: false)
            };

            customize?.Invoke(request);
            return request;
        }
        
        public TemperatureRegionStatsModel GenerateFootStats(PatientTemperatureStatisticGeneratorOptions options, bool isLeft)
        {
            var canGenerateFootStats = CanGenerateFootStats(options, isLeft);
            if (!canGenerateFootStats)
                return null;

            return new TemperatureRegionStatsModel
            {
                Heel = GenerateTemperatureComparison(options),
                Meta1 = GenerateTemperatureComparison(options),
                Meta3 = GenerateTemperatureComparison(options),
                Meta5 = GenerateTemperatureComparison(options),
                BigToe = GenerateTemperatureComparison(options),
            };
        }

        public TemperatureComparisonModel GenerateTemperatureComparison(PatientTemperatureStatisticGeneratorOptions options)
        {
            if (options.HasFlag(PatientTemperatureStatisticGeneratorOptions.GenerateAlwaysRight))
            {
                return new TemperatureComparisonModel
                {
                    Contra = 1,
                    Ipsi = 1
                };
            }   
            else if (options.HasFlag(PatientTemperatureStatisticGeneratorOptions.GenerateAlwaysWrong))
            {
                return new TemperatureComparisonModel
                {
                    Contra = 6,
                    Ipsi = -2
                };
            }

            return new TemperatureComparisonModel
            {
                Contra = DataGenerator.Random.Next(-7, 7) * DataGenerator.Random.NextDouble(),
                Ipsi = DataGenerator.Random.Next(-7, 7) * DataGenerator.Random.NextDouble(),
            };
        }
        
        private static bool CanGenerateFootStats(PatientTemperatureStatisticGeneratorOptions options, bool isLeft)
        {
            var canGenerateForLeftInsole = options.HasFlag(PatientTemperatureStatisticGeneratorOptions.GenerateForLeftInsole);
            var canGenerateForRightInsole = options.HasFlag(PatientTemperatureStatisticGeneratorOptions.GenerateForRightInsole);

            if (canGenerateForLeftInsole && canGenerateForRightInsole)
                return true;

            if (!canGenerateForLeftInsole && !canGenerateForRightInsole)
                return true;

            if (!canGenerateForLeftInsole && isLeft)
                return false;

            if (!canGenerateForRightInsole && !isLeft)
                return false;

            return true;
        }
    }
}