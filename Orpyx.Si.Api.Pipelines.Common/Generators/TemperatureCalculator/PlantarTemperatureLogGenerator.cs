﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Orpyx.IngestorProcessorStorageLibrary;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Generators.TemperatureCalculator
{
    public class PlantarTemperatureLogGenerator : IPipelineGenerator
    {
        public const int ResistanceMinimum = -7;
        public const int ResistanceMaximum = +7;

        public const int CelsiusMinimum = -7;
        public const int CelsiusMaximum = +7;

        public IEnumerable<PlantarTemperatureLogReading> Generate(int hours, int countPerHour, int insoleSide, uint shoePodNumber, DateTimeOffset date, Action<int, int, PlantarTemperatureLogReading> customize = default)
        {
            Assert.Less(countPerHour, TimeConsts.MinutesInHour, $"There can be no more entries than minutes per hour");
            
            for (var h = 0; h < hours; h++)
            {
                var hour = h;
                for (var i = 0; i < countPerHour; i++)
                {
                    var minute = i;

                    void CustomizeEntry(PlantarTemperatureLogReading logReading)
                    {
                        customize?.Invoke(hour, minute, logReading);
                    }

                    yield return Generate(insoleSide, shoePodNumber, date.AddHours(hour).AddMinutes(minute), CustomizeEntry);
                }
            }
        }

        public IEnumerable<PlantarTemperatureLogReading> Generate(int count, int insoleSide, uint shoePodNumber, DateTimeOffset date, Action<int, PlantarTemperatureLogReading> customize = default)
        {
            for (var i = 0; i < count; i++)
            {
                var entryIndex = i;

                void CustomizeEntry(PlantarTemperatureLogReading logReading)
                {
                    customize?.Invoke(entryIndex, logReading);
                }

                yield return Generate(insoleSide, shoePodNumber, date.AddMinutes(i), CustomizeEntry);
            }
        }

        public PlantarTemperatureLogReading Generate(int insoleSide, uint shoePodNumber, DateTimeOffset date, Action<PlantarTemperatureLogReading> customize = default)
        {
            var plantarTemperatureLog = new PlantarTemperatureLogReading(shoePodNumber, date)
            {
                Foot = insoleSide,
                HeelResistance = DataGenerator.Random.Next(ResistanceMinimum, ResistanceMaximum),
                HeelCelsius = DataGenerator.Random.Next(CelsiusMinimum, CelsiusMaximum),
                Meta1Celsius = DataGenerator.Random.Next(CelsiusMinimum, CelsiusMaximum),
                Meta1Resistance = DataGenerator.Random.Next(ResistanceMinimum, ResistanceMaximum),
                Meta3Celsius = DataGenerator.Random.Next(CelsiusMinimum, CelsiusMaximum),
                Meta3Resistance = DataGenerator.Random.Next(ResistanceMinimum, ResistanceMaximum),
                Meta5Celsius = DataGenerator.Random.Next(CelsiusMinimum, CelsiusMaximum),
                Meta5Resistance = DataGenerator.Random.Next(ResistanceMinimum, ResistanceMaximum),
                BigToeCelsius = DataGenerator.Random.Next(CelsiusMinimum, CelsiusMaximum),
                BigToeResistance = DataGenerator.Random.Next(ResistanceMinimum, ResistanceMaximum),
            };

            customize?.Invoke(plantarTemperatureLog);
            return plantarTemperatureLog;
        }
    }
}