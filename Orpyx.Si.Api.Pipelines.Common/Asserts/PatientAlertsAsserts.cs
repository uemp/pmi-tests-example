﻿using System;
using System.Linq;
using NUnit.Framework;
using Orpyx.Si.Business.Patients.Statistics.Summary.Dto;
using Orpyx.Si.Common;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Tests.Common.Models;

namespace Orpyx.Si.Api.Pipelines.Common.Asserts
{
    public static class PatientAlertsAsserts
    {
        public static void AssertZeroModel(this PatientOverallSummaryMonth summary, DateTime month, string message)
        {
            Assert.AreEqual(summary.Month, month, $"{message} | Invalid month date");
            Assert.IsNull(summary.OverallAverages, $"{message} | Invalid {nameof(summary.OverallAverages)}");
            Assert.IsNull(summary.LeftFootRegionAlerts, $"{message} | Invalid {nameof(summary.LeftFootRegionAlerts)}");
            Assert.IsNull(summary.RightFootRegionAlerts, $"{message} | Invalid {nameof(summary.RightFootRegionAlerts)}");
            Assert.IsEmpty(summary.DailySummary, $"{message} | Invalid {nameof(summary.DailySummary)}");
        }

        public static void AssertDailySummary( this PatientOverallSummaryMonth summary, string message, Func<DailyPatientSummary, bool> requiredPositive)
        {
            foreach (var daySummary in summary.DailySummary)
            {
                var isPositive = requiredPositive(daySummary);
                if (isPositive)
                {
                    Assert.Positive(daySummary.Summary.Overall.Alerts, $"Invalid {nameof(daySummary.Summary.Overall.Alerts)} at {daySummary.DateTimeToDay:d} | {message}");
                    Assert.Positive(daySummary.Summary.Overall.ActiveDurationSecs, $"Invalid {nameof(daySummary.Summary.Overall.ActiveDurationSecs)} at {daySummary.DateTimeToDay:d} | {message}");
                    Assert.Positive(daySummary.Summary.Overall.AlertDurationSecs, $"Invalid {nameof(daySummary.Summary.Overall.AlertDurationSecs)} at {daySummary.DateTimeToDay:d} | {message}");
                }
                else
                {
                    Assert.Zero(daySummary.Summary.Overall.Alerts, $"Invalid {nameof(daySummary.Summary.Overall.Alerts)} at {daySummary.DateTimeToDay:d} | {message}");
                    Assert.Zero(daySummary.Summary.Overall.ActiveDurationSecs, $"Invalid {nameof(daySummary.Summary.Overall.ActiveDurationSecs)} at {daySummary.DateTimeToDay:d} | {message}");
                    Assert.Zero(daySummary.Summary.Overall.AlertDurationSecs, $"Invalid {nameof(daySummary.Summary.Overall.AlertDurationSecs)} at {daySummary.DateTimeToDay:d} | {message}");
                }
            }
        }
        
        public static void AssertModel
        (
            this PatientOverallSummaryMonth summary,
            DaysRange range,
            bool includeEndDay,
            string message,
            Func<DailyPatientSummary, bool> requiredPositive
        )
        {
            var days = range.GetDays(includeEndDay);
            if (days.Any())
            {
                Assert.IsNotNull(summary.OverallAverages, $"{message} | Invalid {nameof(summary.OverallAverages)}");
                Assert.IsNotNull(summary.LeftFootRegionAlerts, $"{message} | Invalid {nameof(summary.LeftFootRegionAlerts)}");
                Assert.IsNotNull(summary.RightFootRegionAlerts, $"{message} | Invalid {nameof(summary.RightFootRegionAlerts)}");
                Assert.IsNotNull(summary.DailySummary, $"{message} | Invalid {nameof(summary.DailySummary)}");
                AssertDailySummary(summary, message, requiredPositive);
            }
            else
            {
                Assert.IsNull(summary.OverallAverages, $"{message} | Invalid {nameof(summary.OverallAverages)}");
                Assert.IsNull(summary.LeftFootRegionAlerts, $"{message} | Invalid {nameof(summary.LeftFootRegionAlerts)}");
                Assert.IsNull(summary.RightFootRegionAlerts, $"{message} | Invalid {nameof(summary.RightFootRegionAlerts)}");
                Assert.IsEmpty(summary.DailySummary, $"{message} | Invalid {nameof(summary.DailySummary)}");
            }
        }
        
    }
}