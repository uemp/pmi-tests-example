﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Orpyx.Si.Admin;
using Orpyx.Si.Common;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Tests.Common.Generators;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Business.Actors;
using Orpyx.Si.Domain.DashboardUsers;

namespace Orpyx.Si.Api.Pipelines.Common.Providers
{
    internal sealed class DashboardUserActorProvider
    {
        private readonly ILogger<DashboardUserActorProvider> logger;
        private readonly SiContext dbContext;

        public DashboardUserActorProvider
        (
            SiContext dbContext,
            ILogger<DashboardUserActorProvider> logger
        )
        {
            this.dbContext = dbContext;
            this.logger = logger;
        }

        public async Task<IDoctorActorExecutor> Provide(IServiceProvider rootServiceProvider, UserOutputDto dashboardUserModel)
        {
            var orpyxUser = await dbContext.DashboardUser
                .FirstAsync(q => q.DashboardUserId == dashboardUserModel.Id);
            return await Provide(rootServiceProvider, orpyxUser);
        }

        public async Task<IDoctorActorExecutor> Provide(IServiceProvider rootServiceProvider, int organizationId, DashboardUserTitleType title)
        {
            var orpyxUser = await AddDashboardUser(organizationId, title);
            return await Provide(rootServiceProvider, orpyxUser);
        }

        private async Task<IDoctorActorExecutor> Provide(IServiceProvider rootServiceProvider, DashboardUser dashboardUser)
        {
            Assert.IsNotNull(dashboardUser.B2cobjectId, $"Failed create actor for {dashboardUser.DashboardUserId} | Actor cannot be created for not confirmed user");
            Assert.IsTrue(dashboardUser.IsConfirmed, $"Failed create actor for {dashboardUser.DashboardUserId} | Actor cannot be created for not confirmed user");
            Assert.IsFalse(dashboardUser.IsDisabled, $"Failed create actor for {dashboardUser.DashboardUserId} | Actor cannot be created for deleted user");

            Assert.AreNotEqual(dashboardUser.B2cobjectId, Guid.Empty, $"Failed create actor for {dashboardUser.DashboardUserId} | Invalid {nameof(DashboardUser.B2cobjectId)}");
            
            //    TODO: Need refactoring build actor, need use ActorBuilder
            var actor = new DashboardUserActor(dashboardUser);
            return new DoctorActorExecutor(rootServiceProvider, actor);
        }

        private async Task<DashboardUser> AddDashboardUser(int organizationId, DashboardUserTitleType title)
        {
            logger.LogDebug($"Begin AddDashboardUser({organizationId}, {title})");

            var organization = await dbContext.Organization.FirstOrDefaultAsync(q => q.OrganizationId == organizationId);
            Assert.IsNotNull(organizationId, $"Failed create dashboard user with organizationId: {organizationId} | Invalid {nameof(organization)}");

            var dashboardUserTitle = await dbContext.DashboardUserTitle.FirstOrDefaultAsync(q => q.Type == title);
            Assert.IsNotNull(dashboardUserTitle, $"Failed create dashboard user with title: {title} | Invalid {nameof(dashboardUserTitle)}");

            var user = await dbContext.DashboardUser.AddAsync(new DashboardUser
            (
                firstName: NameGenerators.FirstNames.PickRandom(),
                lastName: NameGenerators.LastNames.PickRandom(),
                email: EmailGenerators.GenerateRandomEmail(),
                organization, dashboardUserTitle
            ));

            user.Entity.B2cobjectId = Guid.NewGuid();
            user.Entity.SetAwaitingConfirmation(DataGenerator.RandomString(8));
            user.Entity.Confirm(user.Entity.ConfirmationToken);

            await dbContext.SaveChangesAsync();
            logger.LogDebug($"Complete AddDashboardUser({organizationId}, {title}), Successfully created dashboard user {user.Entity.DashboardUserId}");

            return user.Entity;
        }
    }
}