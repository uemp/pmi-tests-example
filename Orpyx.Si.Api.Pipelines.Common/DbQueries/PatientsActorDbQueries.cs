﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Admin.Patients.CreatePatient;
using Orpyx.Si.Business.Patients.GetPatientsList.Filters;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain;
using Orpyx.Si.Domain.Patients;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.DbQueries
{
    public sealed class PatientsActorDbQueries
    {
        public Task<Patient> GetPatient(IBaseActorExecutor actor, int patientId)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();
                var patient = await dbContext.Patient.FirstOrDefaultAsync(q => q.PatientId == patientId);
                Assert.NotNull(patient, $"Failed {nameof(GetPatient)}({patientId})");
                return patient;
            });
        }

        public Task<Patient> CreatePatient(PatientFilterType patientType, CreatePatientRequest request, IBaseActorExecutor actor, Action<MobileUser> customize = default)
        {
            if (patientType == PatientFilterType.Active)
                return CreateActivePatient(request, actor, customize);

            if (patientType == PatientFilterType.InactivePatients)
                return CreateInactivePatient(request, actor, customize);

            if (patientType == PatientFilterType.NewPatients)
                return CreateNewPatient(request, actor);


            throw new NotImplementedException($"Patient type {patientType} not implemented");
        }

        private async Task<MobileUser> CretePatientMobileUser(SiContext dbContext)
        {
            var mobileUserEntry = await dbContext.MobileUser.AddAsync(new MobileUser()
            {
                B2cobjectId = Guid.NewGuid(),
                AllowAccessStartDate = DateTime.UtcNow.WithTimeZone(TimeSpan.Zero),
                AllowMarketingContact = true,
            });

            return mobileUserEntry.Entity;
        }

        private Task<Domain.Organizations.Organization> GetOrganizationById(SiContext dbContext, int organizationId)
        {
            return dbContext.Organization.FirstAsync(q => q.OrganizationId == organizationId);
        }

        private Task<Patient> CreateActivePatient(CreatePatientRequest request, IBaseActorExecutor actor, Action<MobileUser> customize)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();

                var organization = await GetOrganizationById(dbContext, request.OrganizationId);
                var mobileUser = await CretePatientMobileUser(dbContext);
                customize?.Invoke(mobileUser);

                var patientEntry = await dbContext.Patient.AddAsync(new Patient(request.ClientPatientIdentifier, request.FirstName, request.LastName, organization, request.RemotePatientMonitoringPatientId, mobileUser));

                await dbContext.SaveChangesAsync();
                return patientEntry.Entity;
            });
        }

        private Task<Patient> CreateInactivePatient(CreatePatientRequest request, IBaseActorExecutor actor, Action<MobileUser> customize)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();

                var organization = await GetOrganizationById(dbContext, request.OrganizationId);

                var mobileUser = await CretePatientMobileUser(dbContext);
                mobileUser.AllowAccessEndDate = DateTime.UtcNow;
                customize?.Invoke(mobileUser);

                var patientEntry = await dbContext.Patient.AddAsync(new Patient(request.ClientPatientIdentifier, request.FirstName, request.LastName, organization, request.RemotePatientMonitoringPatientId, mobileUser)
                {
                    Deleted = true,
                });

                await dbContext.SaveChangesAsync();
                return patientEntry.Entity;
            });
        }

        private Task<Patient> CreateNewPatient(CreatePatientRequest request, IBaseActorExecutor actor)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var dbContext = provider.GetRequiredService<SiContext>();

                var organization = await GetOrganizationById(dbContext, request.OrganizationId);

                var patientEntry = await dbContext.Patient.AddAsync(new Patient(
                    request.ClientPatientIdentifier,
                    request.FirstName,
                    request.LastName,
                    organization,
                    request.RemotePatientMonitoringPatientId)
                );

                await dbContext.SaveChangesAsync();
                return patientEntry.Entity;
            });
        }
    }
}