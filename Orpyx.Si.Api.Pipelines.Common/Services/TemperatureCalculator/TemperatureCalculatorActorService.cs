﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.IngestorProcessorStorageLibrary;
using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Extensions.Patient;
using Orpyx.Si.Api.Pipelines.Common.Generators.TemperatureCalculator;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Tests.Common.Abstractions;
using TemperatureDifference.TemperatureCalculator.Models;

namespace Orpyx.Si.Api.Pipelines.Common.Services.TemperatureCalculator
{
    public class TemperatureCalculatorActorService : IPipelineService
    {
        private readonly PlantarTemperatureLogGenerator plantarTemperatureLogGenerator;
        private readonly TemperatureCalculatorActorClient temperatureCalculatorActorClient;

        public TemperatureCalculatorActorService(TemperatureCalculatorActorClient temperatureCalculatorActorClient, PlantarTemperatureLogGenerator plantarTemperatureLogGenerator)
        {
            this.temperatureCalculatorActorClient = temperatureCalculatorActorClient;
            this.plantarTemperatureLogGenerator = plantarTemperatureLogGenerator;
        }

        public Task CalculatePlantarTemperature(IDoctorActorExecutor actor, int patientId, params DateTime[] datesToProcess)
        {
            return CalculatePlantarTemperature(actor, patientId, datesToProcess.Select(q => q.WithTimeZone(TimeSpan.Zero)).ToArray());
        }
        
        public async Task CalculatePlantarTemperature(IDoctorActorExecutor actor, int patientId, params DateTimeOffset[] datesToProcess)
        {
            var patient =await actor.GetAzurePatientById(patientId);
            await temperatureCalculatorActorClient.CalculatePlantarTemperature(actor, new PatientTemperatureProcessQuery()
            {
                Patient = patient,
                DateToProcess = datesToProcess
            });
        }
        
        public Task CalculatePlantarTemperature(IDoctorActorExecutor actor, PatientTemperatureProcessQuery userId)
        {
            return temperatureCalculatorActorClient.CalculatePlantarTemperature(actor, userId);
        }

        public async Task<PlantarTemperatureLogReading[]> AddPlantarTemperature(IDoctorActorExecutor actor, Guid patientB2CObjectId, params PlantarTemperatureLogReading[] plantarTemperatureLogs)
        {
            var addPlantarTemperatureResult = await temperatureCalculatorActorClient.AddPlantarTemperature(actor, patientB2CObjectId, plantarTemperatureLogs);
            Assert.IsTrue(addPlantarTemperatureResult, $"Failed {nameof(AddPlantarTemperature)}");

            foreach (var log in plantarTemperatureLogs)
            {
                await TestContext.Out.WriteLineAsync($"AddPlantarTemperature(PK: {log.PartitionKey}, Row: {log.RowKey} | {log.DateGenerated:g} | {log.InsoleSerialNumber})");
            }

            return plantarTemperatureLogs;
        }

        public Task<PlantarTemperatureLogReading[]> AddPlantarTemperature
        (
            IDoctorActorExecutor actor,
            Guid patientB2CObjectId,
            int hours,
            int countPerHour,
            int insoleSide,
            uint shoePodNumber,
            DateTimeOffset date,
            Action<int, int, PlantarTemperatureLogReading> customize = default
        )
        {
            var plantarTemperatureLogs = plantarTemperatureLogGenerator.Generate(hours, countPerHour, insoleSide, shoePodNumber, date, customize).ToArray();
            return AddPlantarTemperature(actor, patientB2CObjectId, plantarTemperatureLogs);
        }

        public Task<PlantarTemperatureLogReading[]> AddPlantarTemperature
        (
            IDoctorActorExecutor actor,
            Guid patientB2CObjectId,
            int count,
            int insoleSide,
            uint shoePodNumber,
            DateTimeOffset date,
            Action<int, PlantarTemperatureLogReading> customize = default
        )
        {
            var plantarTemperatureLogs = plantarTemperatureLogGenerator.Generate(count, insoleSide, shoePodNumber, date, customize).ToArray();
            return AddPlantarTemperature(actor, patientB2CObjectId, plantarTemperatureLogs);
        }
    }
}