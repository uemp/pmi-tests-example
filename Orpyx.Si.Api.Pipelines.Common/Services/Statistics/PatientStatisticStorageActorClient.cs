﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Storage.Base.Filters.InDateRange;
using Orpyx.Si.Storage.Patients.Statistics.Data;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Statistics
{
    public sealed class PatientStatisticStorageActorService : IPipelineService
    {
        private readonly PatientStatisticStorageActorClient storageActorClient;

        public PatientStatisticStorageActorService(PatientStatisticStorageActorClient storageActorClient)
        {
            this.storageActorClient = storageActorClient;
        }

        public async Task<IReadOnlyList<PatientStatisticSummary>> GetSummaryStatistic(IDoctorActorExecutor actor, Guid patientB2CObjectId, InDateRangeEntityFilter filter)
        {
            var getSummaryStatisticsResult = await storageActorClient.GetSummaryStatistic(actor, patientB2CObjectId, filter);
            getSummaryStatisticsResult.AssertIsSucceeded($"Failed {nameof(GetSummaryStatistic)}({patientB2CObjectId}, {filter.Predicate})");
            return getSummaryStatisticsResult.Result;
        }

        public async Task AddSummaryStatistic(IDoctorActorExecutor actor, IReadOnlyList<PatientStatisticSummary> statisticSummary)
        {
            var addSummaryStatisticResult = await storageActorClient.AddSummaryStatistic(actor, statisticSummary);
            addSummaryStatisticResult.AssertIsSucceeded($"Failed {nameof(AddSummaryStatistic)}");
        }
    }
}