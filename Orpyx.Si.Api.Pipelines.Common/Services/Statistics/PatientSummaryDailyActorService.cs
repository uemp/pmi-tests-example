﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Storage.UserAlerts.Data;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Statistics
{
    public class PatientSummaryDailyActorService : IPipelineService
    {
        private readonly PatientSummaryDailyActorClient patientSummaryDailyActorClient;

        public PatientSummaryDailyActorService(PatientSummaryDailyActorClient patientSummaryDailyActorClient)
        {
            this.patientSummaryDailyActorClient = patientSummaryDailyActorClient;
        }

        public async Task AddSummaryDaily(IBaseActorExecutor actor, Guid patientId, SummaryDaily request)
        {
            await TestContext.Out.WriteLineAsync($"AddSummaryDaily({request.DateTime:d}, RowKey: {request.RowKey}, RowKey: {request.PartitionKey})");
            
            var result = await patientSummaryDailyActorClient.AddSummaryDaily(actor, patientId, request);
            Assert.IsTrue(result, $"Failed {nameof(AddSummaryDaily)}(patientId: {patientId})");
        }
        
        public async Task AddSummaryDaily(IBaseActorExecutor actor, Guid patientId, IReadOnlyList<SummaryDaily> request)
        {
            var result = await patientSummaryDailyActorClient.AddSummaryDaily(actor, patientId, request);
            Assert.IsTrue(result, $"Failed {nameof(AddSummaryDaily)}(patientId: {patientId})");
        }
        
        public async Task AddSummaryDaily(IBaseActorExecutor actor, Guid patientId, InsertSideDaily request)
        {
            var result = await patientSummaryDailyActorClient.AddSummaryDaily(actor, patientId, request);
            Assert.IsTrue(result, $"Failed {nameof(AddSummaryDaily)}(patientId: {patientId})");
        }        
        
        public async Task AddSummaryDaily(IBaseActorExecutor actor, Guid patientId, IReadOnlyList<InsertSideDaily> request)
        {
            var result = await patientSummaryDailyActorClient.AddSummaryDaily(actor, patientId, request);
            Assert.IsTrue(result, $"Failed {nameof(AddSummaryDaily)}(patientId: {patientId})");
        }
        
    }
}