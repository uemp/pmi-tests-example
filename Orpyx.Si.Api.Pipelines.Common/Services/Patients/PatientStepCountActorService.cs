﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Asserts;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.StepCounts;
using Orpyx.Si.Business.Patients.Statistics.StepCounts.Dto;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Storage.StepCounts.Data;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientStepCountActorService : IPipelineService
    {
        private readonly PatientStepCountActorClient stepCountActorClient;
        private readonly CreateDailyPatientStepCountsGenerator createDailyPatientStepCountsGenerator;
        public PatientStepCountActorService(PatientStepCountActorClient stepCountActorClient, CreateDailyPatientStepCountsGenerator createDailyPatientStepCountsGenerator)
        {
            this.stepCountActorClient = stepCountActorClient;
            this.createDailyPatientStepCountsGenerator = createDailyPatientStepCountsGenerator;
        }

        public async Task<IReadOnlyList<PatientStepCountsSummary>> GetPatientSteps(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            var getPatientStepsResult = await stepCountActorClient.GetPatientSteps(actor, patientId, range);
            getPatientStepsResult.AssertIsSucceeded($"Failed {nameof(GetPatientSteps)} for patient {patientId} in range: {range}");
            getPatientStepsResult.Result.AssertModel($"Failed {nameof(GetPatientSteps)} for patient {patientId} in range: {range}");
            
            return getPatientStepsResult.Result;
        }

        public async Task AddPatientSteps(IDoctorActorExecutor actor, int patientId, int stepsCount, IReadOnlyList<DateTime> days, Action<DailyPatientStepCounts> customize = default)
        {
            var stepCounts = days.Select(monthDay => createDailyPatientStepCountsGenerator.Generate
            (
                patientId,
                monthDay,
                stepsCount
            )).ToArray();

            await AddPatientSteps(actor, stepCounts);
        }
        
        public Task AddPatientSteps(IDoctorActorExecutor actor, int patientId, DateTime date, int stepsCount, Action<DailyPatientStepCounts> customize = default)
        {
            var stepCounts = createDailyPatientStepCountsGenerator.Generate(patientId, date, stepsCount, customize);
            return AddPatientSteps(actor, stepCounts);
        }
        
        public Task AddPatientSteps(IDoctorActorExecutor actor, int patientId, DateTime date, Action<DailyPatientStepCounts> customize = default, int? minSteps = default, int? maxSteps = default)
        {
            var stepCounts = createDailyPatientStepCountsGenerator.Generate(patientId, date, customize, minSteps, maxSteps);
            return AddPatientSteps(actor, stepCounts);
        }
        
        public async Task AddPatientSteps(IDoctorActorExecutor actor, DailyPatientStepCounts stepCounts)
        {
            Assert.IsNotNull(stepCounts);
            
            var addPatientStepsResult = await stepCountActorClient.AddPatientSteps(actor, stepCounts);
            Assert.IsTrue(addPatientStepsResult, $"Failed {nameof(AddPatientSteps)} for patient {stepCounts.PatientId} in StepDate: {stepCounts.StepDate}");
        }        
        
        public async Task AddPatientSteps(IDoctorActorExecutor actor, IReadOnlyList<DailyPatientStepCounts> stepCounts)
        {
            Assert.IsNotNull(stepCounts);
            
            var addPatientStepsResult = await stepCountActorClient.AddPatientSteps(actor, stepCounts);
            Assert.IsTrue(addPatientStepsResult, $"Failed {nameof(AddPatientSteps)}");
        }
    }
}