﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Storage.StepCounts.Data;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientStepCountStorageActorService : IPipelineService
    {
        private readonly PatientStepCountStorageActorClient storageActorClient;

        public PatientStepCountStorageActorService(PatientStepCountStorageActorClient storageActorClient)
        {
            this.storageActorClient = storageActorClient;
        }

        public async Task<IReadOnlyList<HourlyPatientStepCounts>> GetHourlyPatientStepCount(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            var getStepCountsResult = await storageActorClient.GetHourlyPatientStepCount(actor, patientId, range);
            getStepCountsResult.AssertIsSucceeded($"Faile {nameof(GetHourlyPatientStepCount)}({patientId}, {range})");
            return getStepCountsResult.Result;
        }

        public async Task<IReadOnlyList<DailyPatientStepCounts>> GetDailyPatientStepCount(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            var getStepCountsResult = await storageActorClient.GetDailyPatientStepCount(actor, patientId, range);
            getStepCountsResult.AssertIsSucceeded($"Faile {nameof(GetDailyPatientStepCount)}({patientId}, {range})");
            return getStepCountsResult.Result;
        }
    }
}