﻿using System;
using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.CareNotes;
using Orpyx.Si.Business.CareNote.FetchCareNotes;
using Orpyx.Si.Business.CareNote.SubmitCareNote;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Common;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain.Patients.Extensions;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientCareNotesActorService : IPipelineService
    {
        private readonly PatientCareNotesActorClient patientCareNotesActorClient;
        private readonly CreateCareNoteRequestGenerator careNoteRequestGenerator;

        public PatientCareNotesActorService(PatientCareNotesActorClient patientCareNotesActorClient, CreateCareNoteRequestGenerator careNoteRequestGenerator)
        {
            this.patientCareNotesActorClient = patientCareNotesActorClient;
            this.careNoteRequestGenerator = careNoteRequestGenerator;
        }

        public async Task<ListCareNotesResultEntry> AddPatientCareNote(IDoctorActorExecutor actor, SubmitCareNoteRequest request)
        {
            await TestContext.Out.WriteLineAsync($"AddPatientCareNote(ActionTypeId: {request.CarePractitionerActionTypeId}, ActionRequiredAlertIds: {request.ActionRequiredAlertIds.CommaEnumeration()}) | Begin");
            var addPatientCareNoteResult = await patientCareNotesActorClient.AddPatientCareNote(actor, request);

            addPatientCareNoteResult.AssertIsSucceeded($"Failed {nameof(AddPatientCareNote)}(patientId: {request.PatientId})");
            await TestContext.Out.WriteLineAsync($"AddPatientCareNote(ActionTypeId: {request.CarePractitionerActionTypeId}, ActionRequiredAlertIds: {request.ActionRequiredAlertIds.CommaEnumeration()}) | NoteDate: {addPatientCareNoteResult.Result.NoteDate}");
            
            return addPatientCareNoteResult.Result;
        }

        public Task<ListCareNotesResultEntry> AddPatientCareNote(IDoctorActorExecutor actor, int patientId, int[] actionRequiredAlertIds, Action<SubmitCareNoteRequest> customize = default)
        {
            return AddPatientCareNote(actor, patientId, request =>
            {
                request.ActionRequiredAlertIds = actionRequiredAlertIds;
                customize?.Invoke(request);
            });
        }
        
        public Task<ListCareNotesResultEntry> AddPatientCareNote(IDoctorActorExecutor actor, int patientId, int actionRequiredAlertId, Action<SubmitCareNoteRequest> customize = default)
        {
            return AddPatientCareNote(actor, patientId, new[]
            {
                actionRequiredAlertId

            }, customize);
        }
        
        public async Task<ListCareNotesResultEntry> AddPatientCareNote(IDoctorActorExecutor actor, int patientId, Action<SubmitCareNoteRequest> customize = default)
        {
            var request = await careNoteRequestGenerator.Generate(actor, patientId, customize);
            return await AddPatientCareNote(actor, request);
        }

        public async Task<ListCareNotesResult> GetPatientCareNote(IDoctorActorExecutor actor, int patientId, ListCareNotesQuery query)
        {
            var getPatientCareNoteResult = await actor.ExecuteInScope(async provider =>
            {
                 var careNoteFetcher = provider.GetRequiredService<ICareNoteFetcher>();
                 var siContext = provider.GetRequiredService<SiContext>();

                 var patient = await siContext.Patient.GetDtoByIdAsync(patientId);
                 return await careNoteFetcher.FindCareNotes(patient, query);
            });

            getPatientCareNoteResult.AssertIsSucceeded($"Failed {nameof(GetPatientCareNote)}(patientId: {patientId})");
            return getPatientCareNoteResult.Result;
        }
    }
}