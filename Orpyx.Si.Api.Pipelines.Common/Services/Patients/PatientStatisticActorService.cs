﻿using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Server.Dto;
using Orpyx.Si.Business.Patients.Statistics.Summary.Dto;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientStatisticActorService : IPipelineService
    {
        private readonly PatientStatisticActorClient patientStatisticActorClient;

        public PatientStatisticActorService(PatientStatisticActorClient patientStatisticActorClient)
        {
            this.patientStatisticActorClient = patientStatisticActorClient;
        }

        public async Task<PatientOverallSummary> GetPatientSummaryStatistics(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            var getPatientStatisticResult = await patientStatisticActorClient.GetPatientSummaryStatistics(actor, patientId, range);
            getPatientStatisticResult.AssertIsSucceeded($"Failed {nameof(GetPatientSummaryStatistics)}({patientId}, {range})");

            return getPatientStatisticResult.Result;
        }
        
        public async Task<PatientOverallSummary> GetPatientSummaryStatistics(IDoctorActorExecutor actor, GetPatientSummaryStatisticsQuery query)
        {
            var getPatientStatisticResult = await patientStatisticActorClient.GetPatientSummaryStatistics(actor, query);
            getPatientStatisticResult.AssertIsSucceeded($"Failed {nameof(GetPatientSummaryStatistics)}({query.PatientId}, {query.StartDate:d})");

            return getPatientStatisticResult.Result;
        }
    }
}