﻿using System;
using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Asserts;
using Orpyx.Si.Api.Pipelines.Common.Clients.Statistics;
using Orpyx.Si.Api.Pipelines.Common.Dto;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Extensions;
using Orpyx.Si.Api.Pipelines.Common.Extensions.Patient;
using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Statistics;
using Orpyx.Si.Business.Patients.Statistics.Temperature.Dto;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientTemperatureStatisticActorService : IPipelineService
    {
        private readonly PatientTemperatureStatisticActorClient temperatureStatisticActorClient;

        public PatientTemperatureStatisticActorService(PatientTemperatureStatisticActorClient temperatureStatisticActorClient)
        {
            this.temperatureStatisticActorClient = temperatureStatisticActorClient;
        }

        public Task AddPatientTemperatureStatistic
        (
            IDoctorActorExecutor actor,
            int patientId,
            DateTime day,
            PatientTemperatureStatisticGeneratorOptions options,
            Action<AddPatientTemperatureStatisticRequest> customize = default
        )
        {
            return temperatureStatisticActorClient.AddPatientTemperatureStatistic(actor, patientId, day, options, customize);
        }

        public Task AddPatientTemperatureStatistic(IDoctorActorExecutor actor, AddPatientTemperatureStatisticRequest request)
        {
            return temperatureStatisticActorClient.AddPatientTemperatureStatistic(actor, request);
        }

        public async Task<PatientTemperatureSummaryModel> GetPatientTemperatureStatistics(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            var patient = await actor.GetPatientById(patientId);
            return await GetPatientTemperatureStatistics(actor, new GetPatientTemperatureSummaryRequest()
            {
                Patient = patient,
                Range = range
            });
        }

        public async Task<PatientTemperatureSummaryModel> GetPatientTemperatureStatistics(IDoctorActorExecutor actor, GetPatientTemperatureSummaryRequest request)
        {
            var getPatientTemperatureStatisticsResult = await temperatureStatisticActorClient.GetPatientTemperatureStatistics(actor, request);
            getPatientTemperatureStatisticsResult.AssertIsSucceeded($"Failed {nameof(GetPatientTemperatureStatistics)} for patient {request.Patient} | Range: {request.Range}");
            getPatientTemperatureStatisticsResult.Result.AssertModel($"Failed {nameof(GetPatientTemperatureStatistics)} for patient {request.Patient} | Range: {request.Range}");

            return getPatientTemperatureStatisticsResult.Result;
        }
    }
}