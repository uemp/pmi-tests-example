﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Dto;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Business.ActionRequiredAlert;
using Orpyx.Si.Business.ActionRequiredAlert.Models;
using Orpyx.Si.Business.ActionRequiredAlert.Services.Alerts;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientAlertsActorService : IPipelineService
    {
        private readonly PatientAlertsActorClient patientAlertsActorClient;

        public PatientAlertsActorService(PatientAlertsActorClient patientAlertsActorClient)
        {
            this.patientAlertsActorClient = patientAlertsActorClient;
        }

        public Task ClearActionRequiredAlerts(IDoctorActorExecutor actor, int patientId)
        {
            return patientAlertsActorClient.ClearActionRequiredAlerts(actor, patientId);
        }

        public Task<ActionRequiredAlert> CreateActionRequiredAlert(IDoctorActorExecutor actor, CreateActionRequiredAlertRequest request)
        {
            return patientAlertsActorClient.CreateActionRequiredAlert(actor, request);
        }

        public async Task<ListActionRequiredAlertsResult> GetActionRequiredAlerts(IDoctorActorExecutor actor, int patientId)
        {
            var alerts = await patientAlertsActorClient.GetActionRequiredAlerts(actor, patientId);

            //    TODO: Move asserts to assert extension
            Assert.IsNotNull(alerts, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId}");
            Assert.IsNotNull(alerts.Entries, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId}");

            foreach (var alert in alerts.Entries)
            {
                Assert.IsNotNull(alert, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId}");

                if (alert.Type == ActionRequiredAlertTypeEnum.DataReviewWith7Days)
                {
                    Assert.AreEqual(alert.Id, -1, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.Id)}");
                }
                else
                {
                    Assert.Greater(alert.Id, 0, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.Id)}");
                }

                Assert.IsNotNull(alert.AlertMessage, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.AlertMessage)}");
                Assert.IsNotEmpty(alert.AlertMessage, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.AlertMessage)}");

                Assert.IsNotNull(alert.ImageUrl, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.ImageUrl)}");
                Assert.IsNotEmpty(alert.ImageUrl, $"Failed {nameof(GetActionRequiredAlerts)} | {nameof(patientId)}: {patientId} | Invalid {nameof(ActionRequiredAlertsEntry.ImageUrl)}");
            }

            return alerts;
        }

        public Task<ActionRequiredAlert[]> GetRawActionRequiredAlerts(IBaseActorExecutor actor, int patientId)
        {
            return patientAlertsActorClient.GetRawActionRequiredAlerts(actor, patientId);
        }
    }
}