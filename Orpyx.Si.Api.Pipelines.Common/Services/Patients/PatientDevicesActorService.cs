﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Generators.Patients.Devices;
using Orpyx.Si.Business.PatientDevices;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public class PatientDevicesActorService : IPipelineService
    {
        private readonly PatientDevicesActorClient patientDevicesActorClient;
        private readonly CreatePatientDeviceRequestGenerator createPatientDeviceRequestGenerator;

        public PatientDevicesActorService(PatientDevicesActorClient patientDevicesActorClient, CreatePatientDeviceRequestGenerator createPatientDeviceRequestGenerator)
        {
            this.patientDevicesActorClient = patientDevicesActorClient;
            this.createPatientDeviceRequestGenerator = createPatientDeviceRequestGenerator;
        }

        public Task AddPatientDevice(IDoctorActorExecutor actor, Guid patientB2CObjectId, Action<SubmitDeviceData> customize = default)
        {
            var request = createPatientDeviceRequestGenerator.Generate(patientB2CObjectId, customize);
            return AddPatientDevice(actor, request);
        }

        public Task AddPatientDevice(IDoctorActorExecutor actor, Guid patientB2CObjectId, IReadOnlyList<uint> deviceIds, Action<SubmitDeviceData> customize = default)
        {
            var request = createPatientDeviceRequestGenerator.Generate(patientB2CObjectId, deviceIds, customize);
            return AddPatientDevice(actor, request);
        }

        public async Task AddPatientDevice(IDoctorActorExecutor actor, SubmitDeviceData model)
        {
            var addPatientDeviceResult = await patientDevicesActorClient.AddPatientDevice(actor, model);
            addPatientDeviceResult.AssertIsSucceeded($"Failed {nameof(AddPatientDevice)}({model.NameForDevice}) for {model.MobileUserId}");
        }
    }
}