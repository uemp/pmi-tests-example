﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Admin.Patients.CreatePatient;
using Orpyx.Si.Admin.Pipelines.Common.Generators.Patients;
using Orpyx.Si.Api.Pipelines.Common.Clients;
using Orpyx.Si.Api.Pipelines.Common.DbQueries;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Extensions;
using Orpyx.Si.Api.Pipelines.Common.Extensions.Patient;
using Orpyx.Si.Business.Patients.GetPatientInformation;
using Orpyx.Si.Business.Patients.GetPatientsList;
using Orpyx.Si.Business.Patients.GetPatientsList.Filters;
using Orpyx.Si.Common.Pagination;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain;
using Orpyx.Si.Domain.Patients.Extensions;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Extensions;
using Orpyx.Si.Tests.Common.Extensions.Asserts;
using Orpyx.Si.Tests.Common.Generators;

namespace Orpyx.Si.Api.Pipelines.Common.Services.Patients
{
    public sealed class PatientsActorService : IPipelineService
    {
        private readonly CreatePatientRequestGenerator createPatientRequestGenerator;
        private readonly PatientsActorDbQueries patientsActorDbQueries;
        private readonly PatientsActorClient patientsActorClient;
        private readonly GetPatientsRequestGenerator getPatientsRequestGenerator;

        public PatientsActorService(CreatePatientRequestGenerator createPatientRequestGenerator, PatientsActorDbQueries patientsActorDbQueries, PatientsActorClient patientsActorClient,
            GetPatientsRequestGenerator getPatientsRequestGenerator)
        {
            this.createPatientRequestGenerator = createPatientRequestGenerator;
            this.patientsActorDbQueries = patientsActorDbQueries;
            this.patientsActorClient = patientsActorClient;
            this.getPatientsRequestGenerator = getPatientsRequestGenerator;
        }

        public Task<PagingResult<GetPatientsListResponse>> GetPatientsList(IDoctorActorExecutor actor, int organizationId, PatientFilterType patientsType, Action<GetPatientsListRequest> customize = default)
        {
            var request = getPatientsRequestGenerator.Generate(organizationId, patientsType, customize);
            return GetPatientsList(actor, request);
        }

        public async Task<PagingResult<GetPatientsListResponse>> GetPatientsList(IDoctorActorExecutor actor, GetPatientsListRequest request)
        {
            var getPatientsListResult = await patientsActorClient.GetPatientsList(actor, request);
            getPatientsListResult.AssertIsSucceeded($"Failed {nameof(GetPatientsList)}({request.OrganizationId})");
            getPatientsListResult.Result.AssertModel($"Failed {nameof(GetPatientsList)}({request.OrganizationId})");

            foreach (var patient in getPatientsListResult.Result.Items)
            {
                // TODO: Add model asserts
            }

            return getPatientsListResult.Result;
        }

        public async Task<GetPatientInformationResponse> GetPatientInformation(IDoctorActorExecutor actor, GetPatientInformationRequest request)
        {
            var getPatientInformationResult = await patientsActorClient.GetPatientInformation(actor, request);
            getPatientInformationResult.AssertIsSucceeded($"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId})");

            Assert.Greater(getPatientInformationResult.Result.PatientId, 0, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.PatientId)}");

            Assert.IsNotNull(getPatientInformationResult.Result.FirstName, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.FirstName)}");
            Assert.IsNotNull(getPatientInformationResult.Result.FirstName, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.FirstName)}");

            Assert.IsNotNull(getPatientInformationResult.Result.LastName, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.LastName)}");
            Assert.IsNotNull(getPatientInformationResult.Result.LastName, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.LastName)}");

            Assert.IsNotNull(getPatientInformationResult.Result.ClientPatientIdentifier, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.ClientPatientIdentifier)}");
            Assert.IsNotNull(getPatientInformationResult.Result.ClientPatientIdentifier, $"Failed {nameof(GetPatientInformation)}(patientId: {request.PatientId}) | Invalid {nameof(GetPatientInformationResponse.ClientPatientIdentifier)}");

            // TODO: Add model asserts
            return getPatientInformationResult.Result;
        }

        public async Task<GetPatientInformationResponse> GetPatientInformation(IDoctorActorExecutor actor, int patientId)
        {
            var patient = await actor.GetPatientById(patientId);
            return await GetPatientInformation(actor, new GetPatientInformationRequest
            {
                Patient = patient
            });
        }

        public async Task<(int, Guid)> CreateActive
        (
            IDoctorActorExecutor actor,
            DateTimeOffset? authDate = default,
            Action<CreatePatientRequest> customizePatient = default,
            Action<MobileUser> customizeMobileUser = default
        )
        {
            var b2CObjectId = Guid.NewGuid();
            var patientId = await Create
            (
                actor,
                actor.Actor.OrganizationId,
                PatientFilterType.Active,
                customizePatient,
                user =>
                {
                    user.B2cobjectId = b2CObjectId;
                    if (authDate.HasValue)
                    {
                        user.AllowAccessStartDate = authDate.Value;
                    }

                    customizeMobileUser?.Invoke(user);
                }
            );

            return (patientId, b2CObjectId);
        }

        public Task<(int, Guid)> CreateRPMMonitored
        (
            IDoctorActorExecutor actor,
            DateTimeOffset? authDate = default,
            Action<CreatePatientRequest> customizePatient = default,
            Action<MobileUser> customizeMobileUser = default
        )
        {
            return CreateActive(actor, authDate, request =>
            {
                request.RemotePatientMonitoringPatientId = DataGenerator.RandomString(16);
                customizePatient?.Invoke(request);
            }, customizeMobileUser);
        }

        public Task<int> Create(IDoctorActorExecutor actor, int organizationId, PatientFilterType patientType, Action<CreatePatientRequest> customizePatient = default, Action<MobileUser> customizeMobileUser = default)
        {
            var request = createPatientRequestGenerator.Generate(organizationId, customizePatient);
            return Create(actor, patientType, request, customizeMobileUser);
        }

        public async Task<int> Create(IDoctorActorExecutor actor, PatientFilterType patientType, CreatePatientRequest request, Action<MobileUser> customize = default)
        {
            var patient = await patientsActorDbQueries.CreatePatient(patientType, request, actor, customize);
            TestContext.Progress.WriteLine
            (
                $"Created {patientType} patient: {patient.PatientId} [{patient.GetFullName()} / {patient.ClientPatientIdentifier}][RPM: {patient.RemotePatientMonitoringPatientId}] in organization {request.OrganizationId}"
            );


            // TODO: Add model asserts
            return patient.PatientId;
        }
    }
}