﻿using System.Threading.Tasks;
using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Services.AzureFunctions
{
    public class PatientSafePressureAzureFunctionActorService : IPipelineService
    {
        private readonly PatientSafePressureAzureFunctionActorClient patientSafePressureAzureFunctionActorClient;

        public PatientSafePressureAzureFunctionActorService(PatientSafePressureAzureFunctionActorClient patientSafePressureAzureFunctionActorClient)
        {
            this.patientSafePressureAzureFunctionActorClient = patientSafePressureAzureFunctionActorClient;
        }

        //  Not supported for override dates
        public Task ProcessPatients(IBaseActorExecutor actor, params int[] patientIds)
        {
            return patientSafePressureAzureFunctionActorClient.ProcessPatients(actor, patientIds);
        }      
        
        public Task ProcessPatient(IBaseActorExecutor actor, int patientId)
        {
            return patientSafePressureAzureFunctionActorClient.ProcessPatient(actor, patientId);
        }
    }
}