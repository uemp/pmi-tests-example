﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions;
using Orpyx.Si.Common;
using Orpyx.Si.Common.Azure.Functions.Patients;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;
using Orpyx.Si.Tests.Common.Extensions.Asserts;
using TemperatureReviewAlert.Dto;

namespace Orpyx.Si.Api.Pipelines.Common.Services.AzureFunctions
{
    public class PatientTemperatureAzureFunctionActorService : IPipelineService
    {
        private readonly PatientTemperatureAzureFunctionActorClient patientTemperatureAzureFunctionActorClient;

        public PatientTemperatureAzureFunctionActorService(PatientTemperatureAzureFunctionActorClient patientTemperatureAzureFunctionActorClient)
        {
            this.patientTemperatureAzureFunctionActorClient = patientTemperatureAzureFunctionActorClient;
        }

        public async Task ProcessPatients(IBaseActorExecutor actor, int patientId, params DateTimeOffset[] datesToProcess)
        {
            Assert.IsNotEmpty(datesToProcess, $"Required any dates to process");
            
            var patient = await actor.ExecuteInScope(async provider =>
            {
                var patientService = provider.GetRequiredService<IAzurePatientService>();
                var getPatient = await patientService.GetActivePatient(patientId);
                getPatient.AssertIsSucceeded($"Failed {nameof(patientService.GetActivePatient)}({patientId})");
                return getPatient.Result;
            });

            await ProcessPatients(actor, new PatientTemperatureReviewAlertProcessQuery
            {
                Patient = patient,
                DatesToProcess = datesToProcess
            });
        }

        public async Task ProcessPatients(IBaseActorExecutor actor, PatientTemperatureReviewAlertProcessQuery query)
        {
            var result = await patientTemperatureAzureFunctionActorClient.ProcessPatients(actor, query);
            result.AssertIsSucceededOrSkipped($"Failed {nameof(ProcessPatients)}({query})");
        }
    }
}