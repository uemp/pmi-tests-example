﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Storage.StepCounts.Data;
using Orpyx.Si.Storage.StepCounts.Storages;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Clients
{
    public sealed class PatientStepCountStorageActorClient : IPipelineClient
    {
        public async Task<OperationResult<IReadOnlyList<HourlyPatientStepCounts>>> GetHourlyPatientStepCount(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            return await actor.ExecuteInScope(async provider =>
            {
                var storage = provider.GetRequiredService<HourlyPatientStepCountsStorage>();
                return await storage.GetStepCounts(patientId, range);
            });
        }
        
        public async Task<OperationResult<IReadOnlyList<DailyPatientStepCounts>>> GetDailyPatientStepCount(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            return await actor.ExecuteInScope(async provider =>
            {
                var storage = provider.GetRequiredService<DailyPatientStepCountsStorage>();
                return await storage.GetStepCounts(patientId, range);
            });
        }
    }
}