﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Business.Patients.Statistics.StepCounts.Dto;
using Orpyx.Si.Business.Patients.Statistics.StepCounts.Services.Daily;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain.Patients.Extensions;
using Orpyx.Si.Storage.StepCounts.Data;
using Orpyx.Si.Storage.StepCounts.Storages;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Clients
{
    public sealed class PatientStepCountActorClient : IPipelineClient
    {
        public async Task<OperationResult<IReadOnlyList<PatientStepCountsSummary>>> GetPatientSteps(IDoctorActorExecutor actor, int patientId, DateTimeRange range)
        {
            return await actor.ExecuteInScope(async provider =>
            {
                var patient = await provider.GetRequiredService<SiContext>().Patient.GetDtoByIdAsync(patientId);
                var patientStepCountsService = provider.GetRequiredService<IDailyIPatientStepCountsService>();
                return await patientStepCountsService.GetStepCounts(patient, range);
            });
        }
        
        public async Task<bool> AddPatientSteps(IDoctorActorExecutor actor, DailyPatientStepCounts stepCounts)
        {
            return await actor.ExecuteInScope(async provider =>
            {
                var patientStepCountsStorage = provider.GetRequiredService<DailyPatientStepCountsStorage>();
                return await patientStepCountsStorage.AddStepCount(stepCounts);
            });
        }
        
        public async Task<bool> AddPatientSteps(IDoctorActorExecutor actor, IReadOnlyList<DailyPatientStepCounts> stepCounts)
        {
            return await actor.ExecuteInScope(async provider =>
            {
                var patientStepCountsStorage = provider.GetRequiredService<DailyPatientStepCountsStorage>();
                return await patientStepCountsStorage.AddStepCounts(stepCounts);
            });
        }
    }
}