﻿using System.Linq;
using System.Threading.Tasks;
using ActionRequiredAlerts.Pressure.Processor;
using ActionRequiredAlerts.Pressure.Triggers;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Business.ActionRequiredAlert.Services.Alerts;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Common.Dates.Provider;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions
{
    public sealed class PatientSafePressureAzureFunctionActorClient : IPipelineClient
    {
        //  Not supported for override dates
        public Task ProcessPatients(IBaseActorExecutor actor, params int[] patientIds)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var patientPressureProcessTrigger = provider.GetRequiredService<PatientPressureProcessTrigger>();
                await patientPressureProcessTrigger.RunAsync(patientIds);
            });
        }

        public Task ProcessPatient(IBaseActorExecutor actor, int patientId)
        {
            return actor.ExecuteInScope(async provider =>
            {
                //  TODO: Use ActionRequiredAlertTypesStorage from RPX-776
                var alertServices = provider.GetRequiredService<IActionRequiredAlertService>();
                var alertTypes = await alertServices.GetTypes();
                var pressureAlert = alertTypes.First(q => q.Type == ActionRequiredAlertTypeEnum.SafePressureDropsBelow60Percent);

                var currentDateProvider = provider.GetRequiredService<ICurrentDateProvider>();
                var yesterday = currentDateProvider.Now.DateTime.AddDays(-1);
                var patientPressureProcessTrigger = provider.GetRequiredService<PatientSafePressureProcessor>();
                await patientPressureProcessTrigger.Process(new[]
                {
                    patientId
                }, yesterday, pressureAlert);
            });
        }
    }
}