﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Storage.ActionRequiredAlertTypes;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;
using TemperatureReviewAlert.Dto;
using TemperatureReviewAlert.Processors;

namespace Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions
{
    public class PatientTemperatureAzureFunctionActorClient : IPipelineClient
    {
        private readonly ActionRequiredAlertTypesStorage alertTypesStorage;

        public PatientTemperatureAzureFunctionActorClient(ActionRequiredAlertTypesStorage alertTypesStorage)
        {
            this.alertTypesStorage = alertTypesStorage;
        }

        public Task<OperationResult> ProcessPatients(IBaseActorExecutor actor, PatientTemperatureReviewAlertProcessQuery query)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var processor = provider.GetRequiredService<PatientTemperatureReviewAlertProcessor>();
                var results = await processor.Process(query, alertTypesStorage.AlertTypes);
                return results;
            });
        }
    }
}