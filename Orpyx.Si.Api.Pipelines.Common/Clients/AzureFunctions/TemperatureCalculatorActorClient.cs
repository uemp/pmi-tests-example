﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.IngestorProcessorStorageLibrary;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Common.Azure;
using Orpyx.Si.Tests.Common.Abstractions;
using TemperatureDifference.TemperatureCalculator.Models;
using TemperatureDifference.TemperatureCalculator.Triggers;

namespace Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions
{
    public class TemperatureCalculatorActorClient : IPipelineClient
    {
        public Task CalculatePlantarTemperature(IDoctorActorExecutor actor, PatientTemperatureProcessQuery userId)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var trigger = provider.GetRequiredService<PatientTemperatureProcessTrigger>();
                await trigger.CalculatePlantarTemperatureQueueTrigger(userId);
            });
        }

        public Task<bool> AddPlantarTemperature(IDoctorActorExecutor actor, Guid patientB2CObjectId, params PlantarTemperatureLogReading[] plantarTemperatureLogs)
        {
            return actor.ExecuteInScope(async provider =>
            {
                var azureUserCloudStorage = provider.GetRequiredService<IAzureUserCloudStorage>();

                var cloudTableClient = azureUserCloudStorage.Account.CreateCloudTableClient();

                var table = PlantarTemperatureLogReadingTable.GetTable(cloudTableClient, patientB2CObjectId);
                foreach (var log in plantarTemperatureLogs)
                {
                    await table.AddPlantarTemperatureLogReadingAsync(log);
                }

                return await table.Flush();
            });
        }
    }
}    