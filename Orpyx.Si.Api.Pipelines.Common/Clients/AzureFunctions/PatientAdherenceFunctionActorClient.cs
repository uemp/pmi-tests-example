﻿using System;
using System.Threading.Tasks;
using ActionRequiredAlerts.Adherence.Dto;
using ActionRequiredAlerts.Adherence.Triggers;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Orpyx.Si.Api.Pipelines.Common.Storage.ActionRequiredAlertTypes;
using Orpyx.Si.Common;
using Orpyx.Si.Common.Dates;
using Orpyx.Si.Domain.ActionRequiredAlerts;
using Orpyx.Si.Tests.Common.Abstractions;
using Orpyx.Si.Tests.Common.Actors;

namespace Orpyx.Si.Api.Pipelines.Common.Clients.AzureFunctions
{
    public sealed class PatientAdherenceFunctionActorClient : IPipelineClient, IPipelineService
    {
        private readonly ActionRequiredAlertTypesStorage alertTypesStorage;

        public PatientAdherenceFunctionActorClient(ActionRequiredAlertTypesStorage alertTypesStorage)
        {
            this.alertTypesStorage = alertTypesStorage;
        }
        

        public Task<int[]> GetActivePatients(IBaseActorExecutor actor, DateTimeOffset dateOfAuthorizationNotEarlyAt, int[] patientIds)
        {
            Assert.IsNotEmpty(patientIds, $"{nameof(patientIds)} is required, otherwise we will get a list of all patients");

            var alertType = alertTypesStorage.GetBy(ActionRequiredAlertTypeEnum.PatientHasNotBeenWearingDeviceFor3Days);
            return actor.ExecuteInScope(provider =>
            {
                var processTrigger = provider.GetRequiredService<PatientAdherenceProcessPrepareTrigger>();
                return processTrigger.GetActivePatientsWithoutActiveAlert(alertType, dateOfAuthorizationNotEarlyAt, patientIds);
            });
        }

        public async Task ProcessPatients(IBaseActorExecutor actor, params int[] patientIds)
        {
            var result = await actor.ExecuteInScope(provider =>
            {
                var processTrigger = provider.GetRequiredService<PatientAdherenceProcessTrigger>();
                return processTrigger.Process(patientIds);
            });

            Assert.IsNotNull(result, $"Failed {nameof(ProcessPatients)} | Invalid result");
            Assert.IsEmpty(result.GetPatientsWithoutClearedAlertsErrors, $"Failed {nameof(ProcessPatients)} | GetPatientsWithoutClearedAlertsErrors");
            Assert.IsEmpty(result.GetPatientsInNonAdherenceErrors, $"Failed {nameof(ProcessPatients)} | GetPatientsWithoutVitalChangesErrors");
        }
    }
}