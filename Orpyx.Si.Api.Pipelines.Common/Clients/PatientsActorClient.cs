﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orpyx.Si.Api.Pipelines.Common.Executors;
using Orpyx.Si.Api.Pipelines.Common.Extensions;
using Orpyx.Si.Api.Pipelines.Common.Extensions.Patient;
using Orpyx.Si.Business.Patients.GetPatientInformation;
using Orpyx.Si.Business.Patients.GetPatientsList;
using Orpyx.Si.Common.Operations;
using Orpyx.Si.Common.Pagination;
using Orpyx.Si.Database.Context;
using Orpyx.Si.Domain.Patients.Extensions;
using Orpyx.Si.Tests.Common.Abstractions;

namespace Orpyx.Si.Api.Pipelines.Common.Clients
{
    public sealed class PatientsActorClient : IPipelineClient
    {
        public Task<OperationResult<PagingResult<GetPatientsListResponse>>> GetPatientsList(IDoctorActorExecutor actor, GetPatientsListRequest request, Action<GetPatientsListRequest> customize = default)
        {
            customize?.Invoke(request);
            return actor.Execute(request);
        }

        public Task<OperationResult<GetPatientInformationResponse>> GetPatientInformation(IDoctorActorExecutor actor, GetPatientInformationRequest request)
        {
            return actor.Execute(request);
        }

        public async Task<OperationResult<GetPatientInformationResponse>> GetPatientInformation(IDoctorActorExecutor actor, int patientId)
        {
            var patient = await actor.GetPatientById(patientId);
            return await GetPatientInformation(actor, new GetPatientInformationRequest
            {
                Patient = patient
            });
        }
    }
}